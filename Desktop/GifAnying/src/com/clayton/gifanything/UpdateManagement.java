package com.clayton.gifanything;

import java.awt.Component;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Timer;
import java.util.TimerTask;
import java.util.prefs.Preferences;

import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class UpdateManagement implements ActionListener{
	private class UpdateTimerTask extends TimerTask{
		@Override
		public void run() {
			UpdateManagement.this.checkForUpdate();
		}
	}
	private Timer updateTimer;
	private TrayIcon systemTray;
	private Preferences savedPrefs;
	private URI updateLocation;
	
	private String newVersionFileName;
	private String newVersionID;
	public void manualUpdate(Component parent){
		try{
			if(this.isUpdateAvailable()){
				int result = JOptionPane.showConfirmDialog(parent, "GifAnything " + this.newVersionID + " is available would you like to download it?", "Update available", JOptionPane.YES_NO_OPTION);
				if(result == JOptionPane.YES_OPTION){
					try{
						this.update();
					}catch(Exception e){
						e.printStackTrace();
						JOptionPane.showMessageDialog(parent, "Update failed:\n" + e.getMessage(), "Unable to update", JOptionPane.ERROR_MESSAGE);
					}
				}
			} else{
				JOptionPane.showMessageDialog(parent, "No update is available.\n" + this.newVersionID + " is the latest on the update server.","No update available", JOptionPane.INFORMATION_MESSAGE);
			}
		}catch(Exception e){
			e.printStackTrace();
			JOptionPane.showMessageDialog(parent, "Unable to check for updates:\n" + e.getMessage(), "Unable to check for updates", JOptionPane.ERROR_MESSAGE);
		}
		
	}
	
	public static int compareVersionStrings(String str1, String str2){
			String[] vals1 = str1.split("\\.");
			String[] vals2 = str2.split("\\.");
			int i=0;
			while(i<vals1.length && i<vals2.length && vals1[i].equals(vals2[i])) {
				i++;
			}
			
			if (i<vals1.length && i<vals2.length) {
			    int diff = Integer.valueOf(vals1[i]).compareTo(Integer.valueOf(vals2[i]));
			    return Integer.signum(diff);
			}
			
			return Integer.signum(vals1.length - vals2.length);
	}
	public UpdateManagement(TrayIcon systemTray,Preferences systemPrefs){
		this.systemTray = systemTray;
		try {
			this.updateLocation = new URI(Main.UPDATE_LOCATION);
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.savedPrefs = systemPrefs;
	}
	
	public void startUpdateChecking(){
		if(this.updateTimer!=null){
			this.updateTimer.cancel();
			this.updateTimer = new Timer();
		} else {
			updateTimer = new Timer();
		}
		updateTimer.scheduleAtFixedRate(new UpdateTimerTask(), 0, savedPrefs.getLong(Main.UPDATE_CHECK_LENGTH, 1800000));
	}
	
	public void stopUpdateChecking(){
		if(this.updateTimer!=null){
			this.updateTimer.cancel();
			this.updateTimer = null;
		}
	}
	public static String getJavaExec(){
		String os = System.getProperty("os.name").toLowerCase();
		String returnVal = "java";
		if(os.contains("windows")){
			returnVal = "javaw";
		}
		
		return returnVal;
	}
	
	private boolean isUpdateAvailable() throws ParserConfigurationException, MalformedURLException, SAXException, IOException{
		URI xmlFile = this.updateLocation.resolve("updates.xml");
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder;
		dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(xmlFile.toURL().openStream());

		//optional, but recommended
		//read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
		doc.getDocumentElement().normalize();
		NodeList nodes =doc.getElementsByTagName("Version");
		
		Node latestVersion = null;
		for(int j = 0; j < nodes.getLength();j++){
			Node version = nodes.item(j);
			String versionString = version.getAttributes().getNamedItem("id").getNodeValue();
			if(latestVersion == null || compareVersionStrings(newVersionID, versionString) == -1){
				latestVersion = version;
				this.newVersionID = versionString;
				this.newVersionFileName = version.getAttributes().getNamedItem("Location").getNodeValue();
			}
		}
		
		if(compareVersionStrings(this.newVersionID, Main.CURRENT_VERSION_ID) == 1){
			return true;
		}
		return false;
	}
	
	public void checkForUpdate(){
		try {
			if(this.isUpdateAvailable()){
				this.systemTray.displayMessage("Update Available", "GifAnything " + this.newVersionID + " is available. Click here to install it.", TrayIcon.MessageType.INFO);
				this.systemTray.addActionListener(this);
			}
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		update();
		
	}
	private void update() {
		this.systemTray.displayMessage("Downloading Update", "GifAnything will automatically restart when it is done", TrayIcon.MessageType.INFO);
		URI newVersionURI = this.updateLocation.resolve(this.newVersionFileName);
		try {
			InputStream downloadStream = newVersionURI.toURL().openStream();
			
			URL oldFileJar = Main.class.getProtectionDomain().getCodeSource().getLocation();
			File oldFile;
			try {
			  oldFile = new File(oldFileJar.toURI());
			} catch(URISyntaxException e2) {
			  oldFile = new File(oldFileJar.getPath());
			}
			File tempFile = new File(oldFile.getParentFile(), new URI(this.newVersionFileName).getPath());
			
			FileOutputStream outputStream = new FileOutputStream(tempFile);
			int inByte = downloadStream.read();
			while(inByte!= -1){
				outputStream.write(inByte);
				inByte = downloadStream.read();
			}
		
			outputStream.flush();
			outputStream.close();
			downloadStream.close();
			
			Runtime.getRuntime().exec(UpdateManagement.getJavaExec() + " -jar \"" + tempFile.toString() + "\" -ResetStartup -RemoveOldVersion " + oldFile.toString());

			//System.err.println("Deleting " + oldFile.toString());
			Main.exit();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (URISyntaxException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	

}
