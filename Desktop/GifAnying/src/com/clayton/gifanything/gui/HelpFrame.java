package com.clayton.gifanything.gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.prefs.Preferences;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.jnativehook.keyboard.NativeKeyEvent;

import com.clayton.gifanything.Main;

public class HelpFrame extends JFrame
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private LinkLabel txtGifanythingBlog;
	private LinkLabel txtGifanythingBitbucketPage;
	private LinkLabel lnklblSoftwareCredits;

	/**
	 * Create the frame.
	 */
	public HelpFrame(Preferences pref)
	{
		setTitle("Help");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 320, 300);
		setIconImage(Main.Window_Icon);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[] {0};
		gbl_contentPane.rowHeights = new int[] {0, 0, 0, 0};
		gbl_contentPane.columnWeights = new double[]{1.0};
		gbl_contentPane.rowWeights = new double[]{0.0, 0.0, 1.0, 0.0};
		contentPane.setLayout(gbl_contentPane);
		
		txtGifanythingBlog = new LinkLabel();
		txtGifanythingBlog.setText("GifAnything Blog");
		try {
			txtGifanythingBlog.setTarget(new URI("http://GifAnything.tumblr.com"));
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		GridBagConstraints gbc_txtGifanythingBlog = new GridBagConstraints();
		gbc_txtGifanythingBlog.anchor = GridBagConstraints.WEST;
		gbc_txtGifanythingBlog.insets = new Insets(0, 0, 5, 0);
		gbc_txtGifanythingBlog.gridx = 0;
		gbc_txtGifanythingBlog.gridy = 0;
		contentPane.add(txtGifanythingBlog, gbc_txtGifanythingBlog);
		txtGifanythingBlog.setColumns(10);
		
		txtGifanythingBitbucketPage = new LinkLabel();
		txtGifanythingBitbucketPage.setText("GifAnything BitBucket Page");
		try {
			txtGifanythingBitbucketPage.setTarget(new URI("https://bitbucket.org/Fr33dan/gif-anything/"));
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		GridBagConstraints gbc_txtGifanythingBitbucketPage = new GridBagConstraints();
		gbc_txtGifanythingBitbucketPage.anchor = GridBagConstraints.WEST;
		gbc_txtGifanythingBitbucketPage.insets = new Insets(0, 0, 5, 0);
		gbc_txtGifanythingBitbucketPage.gridx = 0;
		gbc_txtGifanythingBitbucketPage.gridy = 1;
		contentPane.add(txtGifanythingBitbucketPage, gbc_txtGifanythingBitbucketPage);
		txtGifanythingBitbucketPage.setColumns(10);

		int modifier = pref.getInt(Main.CAPTURE_MODIFIER, 0);
		int code = pref.getInt(Main.CAPTURE_KEY, NativeKeyEvent.VC_F12);
		String captureKey = (modifier != 0 ? NativeKeyEvent.getModifiersText(modifier) + "+" : "") + NativeKeyEvent.getKeyText(code);
		JLabel lblNewLabel = new JLabel("<html>GifAnything runs in the background waiting to make a GIF of anything you watch by recording your entire screen.  Here is a basic how to:<br>\r\n<br>\r\n1.) Find the content you wish to GIF in the media player of your choice.<br>\r\n2.) When the content starts press and hold " + captureKey + "<br>\r\n3.) When the content ends release " + captureKey + "<br>\r\n4.) Add a caption or crop your GIF in the editor window.<br>\r\n5.) Save your GIF.<br>\r\n<br>\r\nCheck the project blog linked above for updates or check out the BitBucket page for the source code.</html>");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 0);
		gbc_lblNewLabel.fill = GridBagConstraints.BOTH;
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 2;
		contentPane.add(lblNewLabel, gbc_lblNewLabel);
		
		lnklblSoftwareCredits = new LinkLabel();
		lnklblSoftwareCredits.setText("Software Credits");
		try {
			lnklblSoftwareCredits.setTarget(new URI("http://gifanything.tumblr.com/credits"));
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		lnklblSoftwareCredits.setColumns(10);
		GridBagConstraints gbc_lnklblSoftwareCredits = new GridBagConstraints();
		gbc_lnklblSoftwareCredits.anchor = GridBagConstraints.WEST;
		gbc_lnklblSoftwareCredits.gridx = 0;
		gbc_lnklblSoftwareCredits.gridy = 3;
		contentPane.add(lnklblSoftwareCredits, gbc_lnklblSoftwareCredits);
		//this.pack();
	}

}
