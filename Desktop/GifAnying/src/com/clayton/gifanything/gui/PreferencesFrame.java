package com.clayton.gifanything.gui;

import java.awt.Color;
import java.awt.Dialog.ModalityType;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.NumberFormat;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.jnativehook.GlobalScreen;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;

import com.clayton.gifanything.Main;
import com.clayton.gifanything.StartupManagement;
import java.awt.GridLayout;
import javax.swing.DefaultComboBoxModel;

public class PreferencesFrame extends JFrame
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final class NativeKeySetListener implements ActionListener,NativeKeyListener {
		int keyCode;
		JDialog dialog;
		public void actionPerformed(ActionEvent arg0) {
			this.keyCode = savedPref.getInt(Main.CAPTURE_KEY, NativeKeyEvent.VC_F12);
			
			GlobalScreen.getInstance().addNativeKeyListener(this);
			JOptionPane newPane = new JOptionPane("Please press a the new capture key",JOptionPane.PLAIN_MESSAGE);
			dialog = newPane.createDialog(PreferencesFrame.this, "Please press a the new capture key");
			
			dialog.setModalityType(ModalityType.TOOLKIT_MODAL);
			dialog.setVisible(true);
			
			savedPref.putInt(Main.CAPTURE_KEY, this.keyCode);
			PreferencesFrame.this.updateCaptureLabel();
		}

		@Override
		public void nativeKeyPressed(NativeKeyEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void nativeKeyReleased(NativeKeyEvent arg0) {
			this.keyCode = arg0.getKeyCode();
			dialog.setVisible(false);
		}

		@Override
		public void nativeKeyTyped(NativeKeyEvent arg0) {
			// TODO Auto-generated method stub
			
		}
	}
	private final class ModifierMaskListener implements ActionListener {
		private int mask;
		public ModifierMaskListener(int shiftMask) {
			this.mask = shiftMask;
			// TODO Auto-generated constructor stub
		}

		public void actionPerformed(ActionEvent arg0) {
			boolean addShift = ((JCheckBox)arg0.getSource()).isSelected();
			int oldModifier = savedPref.getInt(Main.CAPTURE_MODIFIER, 0);

			// Clear all but the bit in question.
			oldModifier = oldModifier & ~this.mask;
			
			// If we are adding the mask or it onto our modifier.
			if(addShift){
				oldModifier = oldModifier | this.mask;
			}
			
			savedPref.putInt(Main.CAPTURE_MODIFIER, oldModifier);
			PreferencesFrame.this.updateCaptureLabel();
		}
	}
	private JPanel contentPane;
	private JFormattedTextField delayField;
	private JFormattedTextField fpsField;
	private JFormattedTextField widthField;
	private JFormattedTextField heightField;
	private Preferences savedPref;
	private JCheckBox chckbxMaintainAspectRatio;
	private final double aspectRatio;
	private boolean updatingScale;
	private boolean updatingFPS;
	private JLabel lblScaledHeight;
	private JLabel lblScaledWidth;
	private JComboBox<String> comboBox;
	private JLabel lblDefaultFont;
	private JCheckBox chckbxBold;
	private JCheckBox chckbxItalic;
	private JSpinner fontSizeSpinner;
	private JLabel lblSize;
	private JButton btnSetDefaultText;
	private JButton btnNewButton;
	private JSpinner depthSpinner;
	private JLabel lblDefaultBorderDepth;
	private JCheckBox chickenSoupForTheSoul;
	private JPanel captureSettingsPanel;
	private JButton btnSetCaptureKey;
	private JLabel captureKeyLabel;
	private JCheckBox chckbxShift;
	private JCheckBox chckbxCtrl;
	private JCheckBox chckbxAlt;
	private JButton btnCheckForUpdates;
	private JTabbedPane tabbedPane;
	private JPanel updatesPanel;
	private JCheckBox chckbxAutomaticallyCheckFor;
	

	private PropertyChangeListener styleChangeListener = new PropertyChangeListener() {
		public void propertyChange(PropertyChangeEvent arg0) {
			int style = Font.PLAIN;
			
			if(chckbxItalic.isSelected())
				style |= Font.ITALIC;
			
			if(chckbxBold.isSelected())
				style |= Font.BOLD;
			
			PreferencesFrame.this.savedPref.putInt(Main.DEFAULT_CAPTION_FONT_STYLE, style);
		}
	};
	private JLabel lblUpdateCheckInterval;
	private JTextField updateLengthField;
	private JLabel lblMinutes;
	private JLabel lblCurrentVersion;
	private JPanel otherPanel;
	private JLabel lblTargetFileSize;
	private JComboBox fileSizeComboBox;
	/**
	 * Create the frame.
	 */
	public PreferencesFrame(Preferences pref)
	{
		int defaultStyle = pref.getInt(Main.DEFAULT_CAPTION_FONT_STYLE, Font.PLAIN);
		setIconImage(Main.Window_Icon);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				try
				{
					savedPref.flush();
				}
				catch (BackingStoreException e)
				{
					JOptionPane.showMessageDialog(PreferencesFrame.this, "Unable to saved preferences\n" + e.getMessage(), "Unable to save", ERROR);
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});

		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		aspectRatio = ((double)screen.height) / screen.width;
		this.savedPref = pref;
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 325, 442);
		this.setTitle("GifAnything Preferences");
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[] {0};
		gbl_contentPane.rowHeights = new int[] {0};
		gbl_contentPane.columnWeights = new double[]{1.0};
		gbl_contentPane.rowWeights = new double[]{1.0};
		contentPane.setLayout(gbl_contentPane);
		
		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		GridBagConstraints gbc_tabbedPane = new GridBagConstraints();
		gbc_tabbedPane.insets = new Insets(0, 0, 5, 0);
		gbc_tabbedPane.fill = GridBagConstraints.BOTH;
		gbc_tabbedPane.gridx = 0;
		gbc_tabbedPane.gridy = 0;
		contentPane.add(tabbedPane, gbc_tabbedPane);
		
		JPanel monitorPreferencesPane = new JPanel();
		tabbedPane.addTab("Screen Monitor", null, monitorPreferencesPane, null);
		GridBagLayout gbl_monitorPreferencesPane = new GridBagLayout();
		gbl_monitorPreferencesPane.columnWidths = new int[] {0, 0};
		gbl_monitorPreferencesPane.rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0};
		gbl_monitorPreferencesPane.columnWeights = new double[]{0.0, 1.0};
		gbl_monitorPreferencesPane.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
		monitorPreferencesPane.setLayout(gbl_monitorPreferencesPane);
		
		JLabel lblFramesPerSecond = new JLabel("Frames per second:");
		GridBagConstraints gbc_lblFramesPerSecond = new GridBagConstraints();
		gbc_lblFramesPerSecond.insets = new Insets(5, 0, 5, 5);
		gbc_lblFramesPerSecond.anchor = GridBagConstraints.EAST;
		gbc_lblFramesPerSecond.gridx = 0;
		gbc_lblFramesPerSecond.gridy = 0;
		monitorPreferencesPane.add(lblFramesPerSecond, gbc_lblFramesPerSecond);
		
		fpsField = new JFormattedTextField(NumberFormat.getInstance());
		fpsField.addPropertyChangeListener("value", new PropertyChangeListener() {
			public void propertyChange(PropertyChangeEvent arg0) {
				if(!updatingFPS){
					updatingFPS = true;
	
					Number fps = (Number) arg0.getNewValue();
					
					long newDelay = Math.round(1000.0 / fps.doubleValue());
					delayField.setValue(newDelay);
					
					updatingFPS = false;
				}
			}
		});
		GridBagConstraints gbc_fpsField = new GridBagConstraints();
		gbc_fpsField.insets = new Insets(5, 0, 5, 0);
		gbc_fpsField.fill = GridBagConstraints.HORIZONTAL;
		gbc_fpsField.gridx = 1;
		gbc_fpsField.gridy = 0;
		monitorPreferencesPane.add(fpsField, gbc_fpsField);
		fpsField.setColumns(10);
		
		JLabel lblMsPerFrame = new JLabel("Milliseconds per frame:");
		
		GridBagConstraints gbc_lblMsPerFrame = new GridBagConstraints();
		gbc_lblMsPerFrame.anchor = GridBagConstraints.EAST;
		gbc_lblMsPerFrame.insets = new Insets(0, 5, 5, 5);
		gbc_lblMsPerFrame.gridx = 0;
		gbc_lblMsPerFrame.gridy = 1;
		monitorPreferencesPane.add(lblMsPerFrame, gbc_lblMsPerFrame);
		
		delayField = new JFormattedTextField(NumberFormat.getIntegerInstance());
		delayField.addPropertyChangeListener("value", new PropertyChangeListener() {
			public void propertyChange(PropertyChangeEvent arg0) {
				Number delay = (Number) arg0.getNewValue();
				savedPref.putLong(Main.SCREEN_DELAY, delay.longValue());
				if(!updatingFPS){
					updatingFPS = true;
	
					
					double newFPS = 1000.0 / delay.longValue();
					fpsField.setValue(newFPS);
					
					updatingFPS = false;
				}
			}
		});
		delayField.setValue(this.savedPref.getLong(Main.SCREEN_DELAY, 200));
		GridBagConstraints gbc_delayField = new GridBagConstraints();
		gbc_delayField.anchor = GridBagConstraints.NORTH;
		gbc_delayField.fill = GridBagConstraints.HORIZONTAL;
		gbc_delayField.gridx = 1;
		gbc_delayField.gridy = 1;
		monitorPreferencesPane.add(delayField, gbc_delayField);
		delayField.setColumns(10);
		
		final JCheckBox chckbxScaleImages = new JCheckBox("Scale Images");
		chckbxScaleImages.setSelected(savedPref.getBoolean(Main.SCALED_MODE, true));
		chckbxScaleImages.setHorizontalTextPosition(SwingConstants.LEFT);
		chckbxScaleImages.addPropertyChangeListener("BUTTON.BP_CHECKBOX",new PropertyChangeListener() {
			public void propertyChange(PropertyChangeEvent arg0) {
				PreferencesFrame.this.getLblScaledHeight().setEnabled(chckbxScaleImages.isSelected());
				PreferencesFrame.this.getLblScaledWidth().setEnabled(chckbxScaleImages.isSelected());
				PreferencesFrame.this.widthField.setEnabled(chckbxScaleImages.isSelected());
				PreferencesFrame.this.heightField.setEnabled(chckbxScaleImages.isSelected());
				PreferencesFrame.this.chckbxMaintainAspectRatio.setEnabled(chckbxScaleImages.isSelected());
				
				PreferencesFrame.this.savedPref.putBoolean(Main.SCALED_MODE, chckbxScaleImages.isSelected());
			}
		});
		GridBagConstraints gbc_chckbxScaleImages = new GridBagConstraints();
		gbc_chckbxScaleImages.anchor = GridBagConstraints.EAST;
		gbc_chckbxScaleImages.gridwidth = 2;
		gbc_chckbxScaleImages.gridx = 0;
		gbc_chckbxScaleImages.gridy = 2;
		monitorPreferencesPane.add(chckbxScaleImages, gbc_chckbxScaleImages);
		
		lblScaledWidth = new JLabel("Scaled Width:");
		lblScaledWidth.setEnabled(chckbxScaleImages.isSelected());
		GridBagConstraints gbc_lblScaledWidth = new GridBagConstraints();
		gbc_lblScaledWidth.insets = new Insets(0, 0, 5, 5);
		gbc_lblScaledWidth.anchor = GridBagConstraints.EAST;
		gbc_lblScaledWidth.gridx = 0;
		gbc_lblScaledWidth.gridy = 3;
		monitorPreferencesPane.add(lblScaledWidth, gbc_lblScaledWidth);
		
		widthField = new JFormattedTextField(NumberFormat.getIntegerInstance());
		widthField.setEnabled(chckbxScaleImages.isSelected());
		widthField.setValue(savedPref.getInt(Main.SCALED_WIDTH, -1));
		widthField.addPropertyChangeListener("value", new PropertyChangeListener() {
			public void propertyChange(PropertyChangeEvent arg0) {
				Number newWidth = (Number) arg0.getNewValue();
				
				PreferencesFrame.this.savedPref.putInt(Main.SCALED_WIDTH, newWidth.intValue());
				if(!updatingScale){
					updatingScale = true;
					if(PreferencesFrame.this.getChckbxMaintainAspectRatio().isSelected()){
						heightField.setValue((int)(newWidth.intValue() * aspectRatio));
					}
					updatingScale = false;
				}
			}
		});
		GridBagConstraints gbc_widthField = new GridBagConstraints();
		gbc_widthField.insets = new Insets(0, 0, 5, 0);
		gbc_widthField.fill = GridBagConstraints.HORIZONTAL;
		gbc_widthField.gridx = 1;
		gbc_widthField.gridy = 3;
		monitorPreferencesPane.add(widthField, gbc_widthField);
		widthField.setColumns(10);
		
		lblScaledHeight = new JLabel("Scaled Height:");
		lblScaledHeight.setEnabled(chckbxScaleImages.isSelected());
		GridBagConstraints gbc_lblScaledHeight = new GridBagConstraints();
		gbc_lblScaledHeight.anchor = GridBagConstraints.EAST;
		gbc_lblScaledHeight.insets = new Insets(0, 0, 5, 5);
		gbc_lblScaledHeight.gridx = 0;
		gbc_lblScaledHeight.gridy = 4;
		monitorPreferencesPane.add(lblScaledHeight, gbc_lblScaledHeight);
		
		heightField = new JFormattedTextField(NumberFormat.getIntegerInstance());
		heightField.setEnabled(chckbxScaleImages.isSelected());
		heightField.setValue(savedPref.getInt(Main.SCALED_HEIGHT, -1));
		heightField.addPropertyChangeListener("value", new PropertyChangeListener() {
			public void propertyChange(PropertyChangeEvent arg0) {
				Number newHeight = (Number) arg0.getNewValue();
				PreferencesFrame.this.savedPref.putInt(Main.SCALED_HEIGHT, newHeight.intValue());
				if(!updatingScale){
					updatingScale = true;
					
					if(PreferencesFrame.this.getChckbxMaintainAspectRatio().isSelected()){
						widthField.setValue((int)(newHeight.intValue() / aspectRatio));
					}
					updatingScale = false;
				}
			}
		});
		GridBagConstraints gbc_heightField = new GridBagConstraints();
		gbc_heightField.insets = new Insets(0, 0, 5, 0);
		gbc_heightField.fill = GridBagConstraints.HORIZONTAL;
		gbc_heightField.gridx = 1;
		gbc_heightField.gridy = 4;
		monitorPreferencesPane.add(heightField, gbc_heightField);
		heightField.setColumns(10);
		
		chckbxMaintainAspectRatio = new JCheckBox("Maintain Aspect Ratio");
		chckbxMaintainAspectRatio.setSelected(true);
		chckbxMaintainAspectRatio.setHorizontalTextPosition(SwingConstants.LEFT);
		GridBagConstraints gbc_chckbxMaintainAspectRatio = new GridBagConstraints();
		gbc_chckbxMaintainAspectRatio.anchor = GridBagConstraints.EAST;
		gbc_chckbxMaintainAspectRatio.gridwidth = 2;
		gbc_chckbxMaintainAspectRatio.gridx = 0;
		gbc_chckbxMaintainAspectRatio.gridy = 5;
		monitorPreferencesPane.add(chckbxMaintainAspectRatio, gbc_chckbxMaintainAspectRatio);
		
		
		chickenSoupForTheSoul = new JCheckBox("Start on user log in");
		chickenSoupForTheSoul.setSelected(StartupManagement.isStartupItem());
		chickenSoupForTheSoul.setEnabled(StartupManagement.isStartupItemSupported());
		chickenSoupForTheSoul.setHorizontalTextPosition(SwingConstants.LEFT);
		GridBagConstraints gbc_chckbxStartWhenSystem = new GridBagConstraints();
		gbc_chckbxStartWhenSystem.anchor = GridBagConstraints.EAST;
		gbc_chckbxStartWhenSystem.gridwidth = 2;
		gbc_chckbxStartWhenSystem.gridx = 0;
		gbc_chckbxStartWhenSystem.gridy = 6;
		chickenSoupForTheSoul.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				try{
					StartupManagement.setStartupItem(chickenSoupForTheSoul.isSelected());
					
					if(chickenSoupForTheSoul.isSelected()){
						JOptionPane.showMessageDialog(PreferencesFrame.this, "Warning: If you move the program from it's current location it will no longer start on system start.", "Warning", JOptionPane.WARNING_MESSAGE);
					}
				} catch (Exception e){
					chickenSoupForTheSoul.setSelected(false);
					JOptionPane.showMessageDialog(PreferencesFrame.this, e.getMessage(), "Could not add to startup items", JOptionPane.ERROR_MESSAGE);
				}
			}
			
		});
		monitorPreferencesPane.add(chickenSoupForTheSoul, gbc_chckbxStartWhenSystem);
		
		JPanel captionPreferencesPane = new JPanel();
		tabbedPane.addTab("Caption", null, captionPreferencesPane, null);
		GridBagLayout gbl_captionPreferencesPane = new GridBagLayout();
		gbl_captionPreferencesPane.columnWidths = new int[]{0, 0, 0, 0, 0, 0};
		gbl_captionPreferencesPane.rowHeights = new int[]{0, 0, 0, 0, 0};
		gbl_captionPreferencesPane.columnWeights = new double[]{0.0, 0.0, 1.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_captionPreferencesPane.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		captionPreferencesPane.setLayout(gbl_captionPreferencesPane);
		
		lblDefaultFont = new JLabel("Default Font:");
		GridBagConstraints gbc_lblDefaultFont = new GridBagConstraints();
		gbc_lblDefaultFont.insets = new Insets(5, 5, 5, 0);
		gbc_lblDefaultFont.anchor = GridBagConstraints.EAST;
		gbc_lblDefaultFont.gridx = 0;
		gbc_lblDefaultFont.gridy = 0;
		captionPreferencesPane.add(lblDefaultFont, gbc_lblDefaultFont);
		
		comboBox = new JComboBox<String>();
		comboBox.setRenderer(new FontListCellRenderer());
		for(String name : GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames()){
			comboBox.addItem(name);
		}
		comboBox.setSelectedItem(savedPref.get(Main.DEFAULT_CAPTION_FONT_NAME, "Arial"));
		comboBox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
			       if (arg0.getStateChange() == ItemEvent.SELECTED) {
						savedPref.put(Main.DEFAULT_CAPTION_FONT_NAME, (String) arg0.getItem());
			       }
			}
		});
		GridBagConstraints gbc_comboBox = new GridBagConstraints();
		gbc_comboBox.gridwidth = 4;
		gbc_comboBox.insets = new Insets(5, 0, 5, 0);
		gbc_comboBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBox.gridx = 1;
		gbc_comboBox.gridy = 0;
		captionPreferencesPane.add(comboBox, gbc_comboBox);
		
		lblSize = new JLabel("Size:");
		GridBagConstraints gbc_lblSize = new GridBagConstraints();
		gbc_lblSize.anchor = GridBagConstraints.EAST;
		gbc_lblSize.insets = new Insets(0, 0, 5, 5);
		gbc_lblSize.gridx = 1;
		gbc_lblSize.gridy = 1;
		captionPreferencesPane.add(lblSize, gbc_lblSize);
		
		fontSizeSpinner = new JSpinner();
		fontSizeSpinner.setModel(new SpinnerNumberModel(new Integer(savedPref.getInt(Main.DEFAULT_CAPTION_FONT_SIZE, 12)), new Integer(5), null, new Integer(1)));
		fontSizeSpinner.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				Integer newFontSize = (Integer) fontSizeSpinner.getValue();
				
				savedPref.putInt(Main.DEFAULT_CAPTION_FONT_SIZE, newFontSize.intValue());
			}
		});
		GridBagConstraints gbc_fontSizeSpinner = new GridBagConstraints();
		gbc_fontSizeSpinner.fill = GridBagConstraints.HORIZONTAL;
		gbc_fontSizeSpinner.insets = new Insets(0, 0, 5, 5);
		gbc_fontSizeSpinner.gridx = 2;
		gbc_fontSizeSpinner.gridy = 1;
		captionPreferencesPane.add(fontSizeSpinner, gbc_fontSizeSpinner);
		
		chckbxItalic = new JCheckBox("Italic");
		chckbxItalic.setSelected((defaultStyle & Font.ITALIC) != 0);
		chckbxItalic.addPropertyChangeListener(styleChangeListener);
		GridBagConstraints gbc_chckbxItalic = new GridBagConstraints();
		gbc_chckbxItalic.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxItalic.gridx = 3;
		gbc_chckbxItalic.gridy = 1;
		captionPreferencesPane.add(chckbxItalic, gbc_chckbxItalic);
		
		chckbxBold = new JCheckBox("Bold");
		chckbxBold.setSelected((defaultStyle & Font.BOLD) != 0);
		chckbxBold.addPropertyChangeListener(styleChangeListener);
		GridBagConstraints gbc_chckbxBold = new GridBagConstraints();
		gbc_chckbxBold.insets = new Insets(0, 0, 5, 0);
		gbc_chckbxBold.gridx = 4;
		gbc_chckbxBold.gridy = 1;
		captionPreferencesPane.add(chckbxBold, gbc_chckbxBold);
		
		btnNewButton = new JButton("Set default border color");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Color oldCaptionColor = new Color(savedPref.getInt(Main.DEFAULT_CAPTION_COLOR, 0));
				Color newCaptionColor = JColorChooser.showDialog(PreferencesFrame.this,
											                     "Choose Caption Color",
											                     oldCaptionColor);
				if(newCaptionColor!=null)
					savedPref.putInt(Main.DEFAULT_BORDER_COLOR, newCaptionColor.getRGB());
			}
		});
		GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
		gbc_btnNewButton.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnNewButton.gridwidth = 3;
		gbc_btnNewButton.insets = new Insets(0, 0, 5, 5);
		gbc_btnNewButton.gridx = 0;
		gbc_btnNewButton.gridy = 2;
		captionPreferencesPane.add(btnNewButton, gbc_btnNewButton);
		
		btnSetDefaultText = new JButton("Set default text color");
		btnSetDefaultText.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Color oldCaptionColor = new Color(savedPref.getInt(Main.DEFAULT_CAPTION_COLOR, 0));
				Color newCaptionColor = JColorChooser.showDialog(PreferencesFrame.this,
											                     "Choose Caption Color",
											                     oldCaptionColor);
				if(newCaptionColor!=null)
					savedPref.putInt(Main.DEFAULT_CAPTION_COLOR, newCaptionColor.getRGB());
			}
		});
		GridBagConstraints gbc_btnSetDefaultText = new GridBagConstraints();
		gbc_btnSetDefaultText.insets = new Insets(0, 0, 5, 0);
		gbc_btnSetDefaultText.gridwidth = 2;
		gbc_btnSetDefaultText.gridx = 3;
		gbc_btnSetDefaultText.gridy = 2;
		captionPreferencesPane.add(btnSetDefaultText, gbc_btnSetDefaultText);
		
		lblDefaultBorderDepth = new JLabel("Default border depth:");
		GridBagConstraints gbc_lblDefaultBorderDepth = new GridBagConstraints();
		gbc_lblDefaultBorderDepth.anchor = GridBagConstraints.EAST;
		gbc_lblDefaultBorderDepth.gridwidth = 4;
		gbc_lblDefaultBorderDepth.insets = new Insets(0, 0, 0, 5);
		gbc_lblDefaultBorderDepth.gridx = 0;
		gbc_lblDefaultBorderDepth.gridy = 3;
		captionPreferencesPane.add(lblDefaultBorderDepth, gbc_lblDefaultBorderDepth);
		
		depthSpinner = new JSpinner();
		depthSpinner.setModel(new SpinnerNumberModel(savedPref.getInt(Main.DEFAULT_BORDER_DEPTH, 0), 0, 15, 1));
		depthSpinner.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				Integer newFontSize = (Integer) depthSpinner.getValue();
				
				savedPref.putInt(Main.DEFAULT_BORDER_DEPTH, newFontSize.intValue());
			}
		});
		GridBagConstraints gbc_depthSpinner = new GridBagConstraints();
		gbc_depthSpinner.fill = GridBagConstraints.HORIZONTAL;
		gbc_depthSpinner.gridx = 4;
		gbc_depthSpinner.gridy = 3;
		captionPreferencesPane.add(depthSpinner, gbc_depthSpinner);
		
		captureSettingsPanel = new JPanel();
		tabbedPane.addTab("Capture", null, captureSettingsPanel, null);
		GridBagLayout gbl_captureSettingsPanel = new GridBagLayout();
		gbl_captureSettingsPanel.columnWidths = new int[] {0, 0, 0};
		gbl_captureSettingsPanel.rowHeights = new int[]{0, 0, 0, 0};
		gbl_captureSettingsPanel.columnWeights = new double[]{1.0, 1.0, 1.0};
		gbl_captureSettingsPanel.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		captureSettingsPanel.setLayout(gbl_captureSettingsPanel);
		
		captureKeyLabel = new JLabel("New label");
		GridBagConstraints gbc_captureKeyLabel = new GridBagConstraints();
		gbc_captureKeyLabel.gridwidth = 3;
		gbc_captureKeyLabel.anchor = GridBagConstraints.EAST;
		gbc_captureKeyLabel.insets = new Insets(5, 0, 5, 5);
		gbc_captureKeyLabel.gridx = 0;
		gbc_captureKeyLabel.gridy = 0;
		captureSettingsPanel.add(captureKeyLabel, gbc_captureKeyLabel);
		
		btnSetCaptureKey = new JButton("Set Capture Key");
		btnSetCaptureKey.addActionListener(new NativeKeySetListener());
		GridBagConstraints gbc_btnSetCaptureKey = new GridBagConstraints();
		gbc_btnSetCaptureKey.gridwidth = 3;
		gbc_btnSetCaptureKey.insets = new Insets(0, 0, 5, 0);
		gbc_btnSetCaptureKey.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnSetCaptureKey.gridx = 0;
		gbc_btnSetCaptureKey.gridy = 1;
		captureSettingsPanel.add(btnSetCaptureKey, gbc_btnSetCaptureKey);
		
		chckbxCtrl = new JCheckBox("Ctrl");
		GridBagConstraints gbc_chckbxCtrl = new GridBagConstraints();
		gbc_chckbxCtrl.insets = new Insets(0, 0, 0, 5);
		gbc_chckbxCtrl.gridx = 0;
		gbc_chckbxCtrl.gridy = 2;
		chckbxCtrl.setSelected(this.checkModifierMask(NativeKeyEvent.CTRL_MASK));
		chckbxCtrl.addActionListener(new ModifierMaskListener(NativeKeyEvent.CTRL_MASK));
		captureSettingsPanel.add(chckbxCtrl, gbc_chckbxCtrl);
		
		chckbxAlt = new JCheckBox("Alt");
		GridBagConstraints gbc_chckbxAlt = new GridBagConstraints();
		gbc_chckbxAlt.insets = new Insets(0, 0, 0, 5);
		gbc_chckbxAlt.gridx = 1;
		gbc_chckbxAlt.gridy = 2;
		chckbxAlt.setSelected(this.checkModifierMask(NativeKeyEvent.ALT_MASK));
		chckbxAlt.addActionListener(new ModifierMaskListener(NativeKeyEvent.ALT_MASK));
		captureSettingsPanel.add(chckbxAlt, gbc_chckbxAlt);
		
		chckbxShift = new JCheckBox("Shift");
		chckbxShift.addActionListener(new ModifierMaskListener(NativeKeyEvent.SHIFT_MASK));
		GridBagConstraints gbc_chckbxShift = new GridBagConstraints();
		gbc_chckbxShift.gridx = 2;
		gbc_chckbxShift.gridy = 2;
		chckbxShift.setSelected(this.checkModifierMask(NativeKeyEvent.SHIFT_MASK));
		captureSettingsPanel.add(chckbxShift, gbc_chckbxShift);
		
		updatesPanel = new JPanel();
		tabbedPane.addTab("Updates", null, updatesPanel, null);
		GridBagLayout gbl_updatesPanel = new GridBagLayout();
		gbl_updatesPanel.columnWidths = new int[] {0, 0, 0};
		gbl_updatesPanel.rowHeights = new int[] {0, 0, 0, 0, 0};
		gbl_updatesPanel.columnWeights = new double[]{1.0, 0.0, 0.0};
		gbl_updatesPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 1.0};
		updatesPanel.setLayout(gbl_updatesPanel);
		
		chckbxAutomaticallyCheckFor = new JCheckBox("Automatically check for updates");
		chckbxAutomaticallyCheckFor.setSelected(savedPref.getBoolean(Main.CHECK_FOR_UPDATES, true));
		chckbxAutomaticallyCheckFor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				savedPref.putBoolean(Main.CHECK_FOR_UPDATES, chckbxAutomaticallyCheckFor.isSelected());
			}
		});
		
		lblCurrentVersion = new JLabel("Current Version: " + Main.CURRENT_VERSION_ID);
		GridBagConstraints gbc_lblCurrentVersion = new GridBagConstraints();
		gbc_lblCurrentVersion.anchor = GridBagConstraints.EAST;
		gbc_lblCurrentVersion.gridwidth = 3;
		gbc_lblCurrentVersion.insets = new Insets(5, 0, 5, 5);
		gbc_lblCurrentVersion.gridx = 0;
		gbc_lblCurrentVersion.gridy = 0;
		updatesPanel.add(lblCurrentVersion, gbc_lblCurrentVersion);
		GridBagConstraints gbc_chckbxAutomaticallyCheckFor = new GridBagConstraints();
		gbc_chckbxAutomaticallyCheckFor.gridwidth = 3;
		gbc_chckbxAutomaticallyCheckFor.insets = new Insets(0, 0, 5, 0);
		gbc_chckbxAutomaticallyCheckFor.anchor = GridBagConstraints.NORTHEAST;
		gbc_chckbxAutomaticallyCheckFor.gridx = 0;
		gbc_chckbxAutomaticallyCheckFor.gridy = 1;
		updatesPanel.add(chckbxAutomaticallyCheckFor, gbc_chckbxAutomaticallyCheckFor);
		
		lblUpdateCheckInterval = new JLabel("Update Check Interval:");
		GridBagConstraints gbc_lblUpdateCheckInterval = new GridBagConstraints();
		gbc_lblUpdateCheckInterval.anchor = GridBagConstraints.EAST;
		gbc_lblUpdateCheckInterval.insets = new Insets(0, 0, 5, 5);
		gbc_lblUpdateCheckInterval.gridx = 0;
		gbc_lblUpdateCheckInterval.gridy = 2;
		updatesPanel.add(lblUpdateCheckInterval, gbc_lblUpdateCheckInterval);
		
		updateLengthField = new JTextField();
		updateLengthField.setHorizontalAlignment(SwingConstants.TRAILING);
		updateLengthField.setText(Double.toString(savedPref.getLong(Main.UPDATE_CHECK_LENGTH, 1800000) / (60.0 * 1000)));
		updateLengthField.addPropertyChangeListener(new PropertyChangeListener() {
			public void propertyChange(PropertyChangeEvent arg0) {
				double minutes = Double.parseDouble(updateLengthField.getText());
				savedPref.putLong(Main.UPDATE_CHECK_LENGTH, (long)(minutes * 60 * 1000));
			}
		});
		GridBagConstraints gbc_updateLengthField = new GridBagConstraints();
		gbc_updateLengthField.insets = new Insets(0, 0, 5, 5);
		gbc_updateLengthField.fill = GridBagConstraints.HORIZONTAL;
		gbc_updateLengthField.gridx = 1;
		gbc_updateLengthField.gridy = 2;
		updatesPanel.add(updateLengthField, gbc_updateLengthField);
		updateLengthField.setColumns(5);
		
		lblMinutes = new JLabel("Minutes");
		GridBagConstraints gbc_lblMinutes = new GridBagConstraints();
		gbc_lblMinutes.insets = new Insets(0, 0, 5, 5);
		gbc_lblMinutes.gridx = 2;
		gbc_lblMinutes.gridy = 2;
		updatesPanel.add(lblMinutes, gbc_lblMinutes);
		
		btnCheckForUpdates = new JButton("Check for updates now");
		GridBagConstraints gbc_btnCheckForUpdates = new GridBagConstraints();
		gbc_btnCheckForUpdates.gridwidth = 3;
		gbc_btnCheckForUpdates.insets = new Insets(0, 0, 5, 0);
		gbc_btnCheckForUpdates.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnCheckForUpdates.anchor = GridBagConstraints.NORTH;
		gbc_btnCheckForUpdates.gridx = 0;
		gbc_btnCheckForUpdates.gridy = 3;
		updatesPanel.add(btnCheckForUpdates, gbc_btnCheckForUpdates);
		
		otherPanel = new JPanel();
		tabbedPane.addTab("Other", null, otherPanel, null);
		GridBagLayout gbl_otherPanel = new GridBagLayout();
		gbl_otherPanel.columnWidths = new int[]{0, 0, 0};
		gbl_otherPanel.rowHeights = new int[]{0, 0};
		gbl_otherPanel.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		gbl_otherPanel.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		otherPanel.setLayout(gbl_otherPanel);
		
		lblTargetFileSize = new JLabel("Target File Size:");
		GridBagConstraints gbc_lblTargetFileSize = new GridBagConstraints();
		gbc_lblTargetFileSize.insets = new Insets(0, 0, 0, 5);
		gbc_lblTargetFileSize.anchor = GridBagConstraints.EAST;
		gbc_lblTargetFileSize.gridx = 0;
		gbc_lblTargetFileSize.gridy = 0;
		otherPanel.add(lblTargetFileSize, gbc_lblTargetFileSize);
		
		fileSizeComboBox = new JComboBox();
		fileSizeComboBox.setModel(new DefaultComboBoxModel(new String[] {"512 KB", "1 MB", "2 MB", "Other"}));
		fileSizeComboBox.setSelectedIndex(1);
		fileSizeComboBox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if(arg0.getStateChange()== ItemEvent.SELECTED){
					long newSize = 0;
					if(fileSizeComboBox.getItemCount() > 4 && fileSizeComboBox.getSelectedIndex() != 4){
						fileSizeComboBox.removeItemAt(4);
					}
					switch(((String)fileSizeComboBox.getSelectedItem())){
					case "512 KB":
						newSize = 512*1024;
						break;
					case "1 MB":
						newSize = 1024*1024;
						break;
					case "2 MB":
						newSize = 1024*1024*2;
						break;
					case "Other":
						try{
							String result = JOptionPane.showInputDialog(PreferencesFrame.this, "Please enter a size in bytes", "Select a size", JOptionPane.QUESTION_MESSAGE);
							if(result !=null){
								long newValue = Long.parseLong(result);
								newSize = newValue;
								String newObject = newValue + " B";
								fileSizeComboBox.addItem(newObject);
								fileSizeComboBox.setSelectedItem(newObject);
							}
						} catch(Exception e){
							e.printStackTrace();
							JOptionPane.showMessageDialog(PreferencesFrame.this, "Could not set target filesize", "Error", JOptionPane.ERROR_MESSAGE);
						}
						break;
					}
					if(newSize !=0){
						if(Main.log)
							System.err.println(newSize);
						PreferencesFrame.this.savedPref.putLong(Main.TARGET_FILESIZE, newSize);
						try {
							PreferencesFrame.this.savedPref.flush();
						} catch (BackingStoreException e) {
							if(Main.log)
								e.printStackTrace();
						}
					}
				}
			}
		});
		GridBagConstraints gbc_fileSizeComboBox = new GridBagConstraints();
		gbc_fileSizeComboBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_fileSizeComboBox.gridx = 1;
		gbc_fileSizeComboBox.gridy = 0;
		otherPanel.add(fileSizeComboBox, gbc_fileSizeComboBox);
		btnCheckForUpdates.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Main.getUpdateManager().manualUpdate(PreferencesFrame.this);
			}
		});
		
		
		
		this.updateCaptureLabel();
		this.pack();
	}
	
	private boolean checkModifierMask(int mask){
		int modifier = savedPref.getInt(Main.CAPTURE_MODIFIER, 0);
		return (modifier & mask) != 0;
	}
	
	private void updateCaptureLabel(){
		int modifier = savedPref.getInt(Main.CAPTURE_MODIFIER, 0);
		int code = savedPref.getInt(Main.CAPTURE_KEY, NativeKeyEvent.VC_F12);
		
		this.captureKeyLabel.setText("Capture Key: " + (modifier != 0 ? NativeKeyEvent.getModifiersText(modifier) + "+" : "") + NativeKeyEvent.getKeyText(code));
	}

	public JCheckBox getChckbxMaintainAspectRatio() {
		return chckbxMaintainAspectRatio;
	}
	protected JLabel getLblScaledHeight() {
		return lblScaledHeight;
	}
	protected JLabel getLblScaledWidth() {
		return lblScaledWidth;
	}
}
