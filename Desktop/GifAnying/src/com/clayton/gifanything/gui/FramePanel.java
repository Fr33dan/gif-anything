package com.clayton.gifanything.gui;

import javax.swing.JPanel;
import javax.swing.SwingConstants;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridBagLayout;
import java.awt.Image;

import javax.swing.JCheckBox;

import java.awt.GridBagConstraints;

import javax.swing.JTextField;

import java.awt.Insets;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.image.BufferedImage;
import java.beans.Beans;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JLabel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;

import com.clayton.gifanything.Main;

public class FramePanel extends JPanel implements FrameInfoChangeListener, ComponentListener,Runnable{
	private final GifPanel gifPanel;
	private final int index;
	private final FrameInfo info;
	private final JCheckBox chckbxEnabled;
	private final JTextField txtTime;
	private final BufferedImage originalFrame;
	private final double aspectRatio;
	private volatile Image scaledImage;
	private Thread scalingThread;
	private double scaleFactor;
	/**
	 * Create the panel.
	 */
	public FramePanel(GifPanel panel,int index) {
		this.gifPanel = panel;
		this.originalFrame = this.gifPanel.getOriginalFrames()[index];
		this.index = index;
		this.info = this.gifPanel.getFrameInfo(this.index);
		aspectRatio = ((double)this.originalFrame.getWidth()) / this.originalFrame.getHeight();
		
		if(!Beans.isDesignTime()){
			this.addComponentListener(this);
			this.setScaleFactor(.25);
		}
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0};
		gridBagLayout.columnWeights = new double[]{1.0, 0.0};
		gridBagLayout.rowWeights = new double[]{0.0, 1.0, 0.0};
		setLayout(gridBagLayout);
		
		chckbxEnabled = new JCheckBox("");
		chckbxEnabled.setSelected(this.info.isEnabled());
		chckbxEnabled.setHorizontalTextPosition(SwingConstants.LEFT);
		chckbxEnabled.addPropertyChangeListener(new PropertyChangeListener() {
			public void propertyChange(PropertyChangeEvent arg0) {
				if(info.isEnabled() != chckbxEnabled.isSelected()){
					info.setEnabled(chckbxEnabled.isSelected());
					

					if(info.isEnabled()){
						FramePanel.this.txtTime.setEnabled(true);
						FrameInfo affectedFrame = info.getAffectedFrame();
						do{
							affectedFrame.setDelay(affectedFrame.getDelay() - info.getDelay());
							affectedFrame = affectedFrame.getAffectedFrame();
						} while(affectedFrame != null);
						info.setAffectedFrame(null);
					} else{
						FramePanel.this.txtTime.setEnabled(false);
						FrameInfo lastFrame = info;
						if(info.getFrame() != 0){
							lastFrame = gifPanel.getFrameInfo(lastFrame.getFrame() - 1);
							while(!lastFrame.isEnabled() && lastFrame.getFrame() != 0){
								lastFrame = gifPanel.getFrameInfo(lastFrame.getFrame() - 1);
							}
						}
						lastFrame.setDelay(lastFrame.getDelay() + info.getDelay());
						info.setAffectedFrame(lastFrame);
					}
				}
			}
		});
		GridBagConstraints gbc_chckbxEnabled = new GridBagConstraints();
		gbc_chckbxEnabled.anchor = GridBagConstraints.EAST;
		gbc_chckbxEnabled.insets = new Insets(0, 0, 5, 0);
		gbc_chckbxEnabled.gridx = 1;
		gbc_chckbxEnabled.gridy = 0;
		add(chckbxEnabled, gbc_chckbxEnabled);
		
		txtTime = new JTextField();
		txtTime.setText(Long.toString(info.getDelay()));
		GridBagConstraints gbc_txtTime = new GridBagConstraints();
		gbc_txtTime.anchor = GridBagConstraints.EAST;
		gbc_txtTime.gridx = 1;
		gbc_txtTime.gridy = 2;
		add(txtTime, gbc_txtTime);
		txtTime.setColumns(4);
		txtTime.getDocument().addDocumentListener(new DocumentListener(){

			public void update(DocumentEvent arg0){
				try
				{
					String newValue = arg0.getDocument().getText(0, arg0.getDocument().getLength());
					if(!newValue.equals("") && gifPanel != null){
						if(newValue.toLowerCase().endsWith("ms"))
							newValue = newValue.substring(0, newValue.length() - 2);
						
						long newScalingValue = Long.parseLong(newValue);
						FramePanel.this.info.setDelay(newScalingValue);
					}
				}
				catch (BadLocationException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			@Override
			public void changedUpdate(DocumentEvent arg0)
			{
				update(arg0);
			}

			@Override
			public void insertUpdate(DocumentEvent arg0)
			{	
				update(arg0);
			}

			@Override
			public void removeUpdate(DocumentEvent arg0)
			{	
				update(arg0);
			}
			
		});
		this.info.addChangeListener(this);
	}
	public double getScaleFactor() {
		return scaleFactor;
	}
	public void setScaleFactor(double scaleFactor) {
		this.scaleFactor = scaleFactor;
		Dimension newSize = new Dimension((int)(this.originalFrame.getWidth() * this.scaleFactor), (int)(this.originalFrame.getHeight() * this.scaleFactor));
		this.setPreferredSize(newSize);
		this.setMaximumSize(newSize);
		this.setMaximumSize(newSize);
	}
	@Override
	public void frameInfoChanged(FrameInfoChangeEvent i) {
		chckbxEnabled.setSelected(this.info.isEnabled());
		txtTime.setText(Long.toString(info.getDelay()));
		if(Main.log)
			System.err.println(System.currentTimeMillis() + " : FramePanel changed");
	}
	@Override
	public void componentHidden(ComponentEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void componentMoved(ComponentEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void componentResized(ComponentEvent arg0) {
		if(this.scalingThread!= null)
			this.scalingThread.interrupt();
		this.scalingThread = new Thread(this);
		this.scalingThread.start();
	}
	@Override
	public void componentShown(ComponentEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		if(Main.log)
			System.err.println(System.currentTimeMillis() + " : FramePanel Painted");
		if(this.scaledImage != null){
			if(aspectRatio > 1){
				int overhead = this.getHeight() - this.scaledImage.getHeight(null);
				int inset = overhead /2;
				g.drawImage(this.scaledImage, 0, inset, null);
			} else{
				int overhead = this.getWidth() - this.scaledImage.getWidth(null);
				int inset = overhead /2;
				g.drawImage(this.scaledImage, inset, 0, null);
			}
		}
	}
	@Override
	public void run() {
		//this.scaledImage = null;
		int newWidth,newHeight;
		if(aspectRatio > 1){
			newWidth = this.getWidth();
			newHeight = (int) (newWidth * (1 / aspectRatio));
		} else{
			newHeight = this.getHeight();
			newWidth = (int) (newHeight * aspectRatio);
		}
		Image newImage = this.originalFrame.getScaledInstance(newWidth, newHeight, BufferedImage.SCALE_SMOOTH);
		
		if(!Thread.interrupted()){
			this.scaledImage = newImage;
			this.repaint();
		}
	}

}
