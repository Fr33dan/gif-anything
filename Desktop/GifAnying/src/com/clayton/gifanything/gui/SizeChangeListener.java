package com.clayton.gifanything.gui;

import java.awt.Dimension;

public interface SizeChangeListener{
	public void SizeChanged(int newFileSize, Dimension imageSize);
}