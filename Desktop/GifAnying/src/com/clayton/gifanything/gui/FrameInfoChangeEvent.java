package com.clayton.gifanything.gui;

import java.util.EventObject;

public class FrameInfoChangeEvent extends EventObject {

	public FrameInfoChangeEvent(FrameInfo arg1) {
		super(arg1);
	}

	public FrameInfo getNewInfo() {
		return (FrameInfo)this.getSource();
	}


}
