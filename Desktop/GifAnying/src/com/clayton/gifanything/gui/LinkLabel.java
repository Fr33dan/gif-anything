package com.clayton.gifanything.gui;
/* License - LGPL
LinkLabel Using the Desktop Class

There are a lot of link labels.  This one uses the Java 1.6+ 
Desktop class for its functionality, and is thereby suitable 
for applets, applications launched using JWS, and 'standard' 
(non-JWS) desktop applications.

Note that the Desktop class can be used with pre 1.6 JREs. It 
is available through JDIC. 
https://jdic.dev.java.net/nonav/documentation/javadoc/jdic/org/jdesktop/jdic/desktop/Desktop.html
*/

import java.awt.Desktop;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.*;

import javax.swing.JTextField;
import javax.swing.JPanel;
import javax.swing.JOptionPane;
import javax.swing.border.MatteBorder;
import javax.swing.border.Border;

import java.net.URI;
import java.io.File;

/**
A Java 1.6+ LinkLabel that uses the Desktop class for opening
the document of interest.

The Desktop.browse(URI) method can be invoked from applications,
applets and apps. launched using Java Webstart.  In the latter
two cases, the usual fall-back methods are used for sandboxed apps
(see the JavaDocs for further details).

While called a 'label', this class actually extends JTextField,
to easily allow the component to become focusable using keyboard
navigation.

To successfully browse to a URI for a local File, the file name
must be constructed using a canonical path.

@author Andrew Thompson, Joseph Tignor
@version 2014/01/21
*/
public class LinkLabel
    // we extend a JTextField, to get a focusable component
    extends JTextField
    implements MouseListener, FocusListener, ActionListener {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** The target or href of this link. */
    private URI target;

    private Color standardColor = new Color(0,0,255);
    private Color hoverColor = new Color(255,0,0);
    private Color activeColor = new Color(128,0,128);
    private Color transparent = new Color(0,0,0,0);

    private boolean underlineVisible = true;

    private Border activeBorder;
    private Border hoverBorder;
    private Border standardBorder;

    /**
     * Construct a LinkLabel that points to the given target.
     * The URI will be used as the link text.
     * @param target URI that this link should point to.
     */
    public LinkLabel(URI target) {
        this( target, target.toString() );
    }

    /**
     * Construct a LinkLabel that points to the given target,
     * and displays the text to the user.
     * @param target URI that this link should point to.
     * @param text Text to display to the user.
     */
    public LinkLabel(URI target, String text) {
        this(text);
        this.setTarget(target);
    }
    
    /**
     * Construct a LinkLabel that without a target and displays the text to the user.
     * @param text Text to display to the user.
     */
    public LinkLabel(String text){
    	super(text);
    	init();
    	adjustSize();
    }

    /**
     * Constructs a LinkLabel without a target or text.
     */
    public LinkLabel(){
    	super();
    	init();
    }
    
    private void adjustSize(){
    	FontMetrics m = this.getFontMetrics(this.getFont());
    	Insets margin = this.getMargin();
    	int insetWidth = this.getInsets().left + this.getInsets().right + margin.left + margin.right;
    	int insetHeight = this.getInsets().top + this.getInsets().bottom + margin.top + margin.bottom;
    	Dimension newSize = new Dimension(m.stringWidth(this.getText()) + insetWidth, m.getHeight() + insetHeight);
    	this.setPreferredSize(newSize);
    	this.setMinimumSize(newSize);
    }
    
    @Override
    public void setText(String text){
    	super.setText(text);
    	this.adjustSize();
    }

    /**
     * Get the active color for this link (default is purple).
     * @return Color the link should be when active.
     */
    public Color getActiveColor() {
        return activeColor;
    }

    /**
     * Set the active color for this link (default is purple).
     * @param active Color the link should be when active.
     */
    public void setActiveColor(Color active) {
        activeColor = active;
        this.colorSetup();
    }

    /**
     * Get the hover/focused color for this link (default is red).
     * @return Color the link should be when it is being hovered over.
     */
    public Color getHoverColor() {
        return hoverColor;
    }
    
    /**
     * Set the hover/focused color for this link (default is red).
     * @param hover Color the link should be when it is being hovered over.
     */
    public void setHoverColor(Color hover) {
        hoverColor = hover;
        this.colorSetup();
    }
    
    /**
     * Get the standard (non-focused, non-active) color for this
     * link (default is blue).
     * @return Color the link is when it is not active or focused
     */
    public Color getStandardColor() {
        return standardColor;
    }
    
    /**
     * Set the standard (non-focused, non-active) color for this
     * link (default is blue).
     * @param standard Color the link should be when it is not active or focused
     */
    public void setStandardColor(Color standard) {
        standardColor = standard;
        this.colorSetup();
    }

    /**
     * Get whether or not the link is underlined
     * @return Whether or not the link is underlined
     */
    public boolean isUnderlineVisible() {
        return this.underlineVisible;
    }
    
    /**
     * Set whether or not the link is underlined
     * @param underlineVisible Whether or not the link is underlined
     */
    public void setUnderlineVisible(boolean underlineVisible) {
        this.underlineVisible = underlineVisible;
        this.colorSetup();
    }

    /**
     * Gets the current URI target of this link.
     * @return Current URI target of this link points to.
     */
    public URI getTarget() {
		return target;
	}

    /**
     * Sets the current URI target of this link.
     * @param target URI that this link should point to.
     */
	public void setTarget(URI target) {
        setToolTipText(target.toString());
		this.target = target;
	}

	/**
	 * Add the listeners, configure the field to look and act
     * like a link.
     */
    private void init() {
        this.addMouseListener(this);
        this.addFocusListener(this);
        this.addActionListener(this);

        // make it appear like a label/link
        setEditable(false);
        setCursor( new Cursor(Cursor.HAND_CURSOR) );
        colorSetup();
    }
    
    /** 
     * Applies the color properties of the LinkLabel.
     */
    private void colorSetup(){
        setForeground(standardColor);
        setBorder(standardBorder);
        
        if (underlineVisible) {
            activeBorder = new MatteBorder(0,0,1,0,activeColor);
            hoverBorder = new MatteBorder(0,0,1,0,hoverColor);
            standardBorder = new MatteBorder(0,0,1,0,transparent);
        } else {
            activeBorder = new MatteBorder(0,0,0,0,activeColor);
            hoverBorder = new MatteBorder(0,0,0,0,hoverColor);
            standardBorder = new MatteBorder(0,0,0,0,transparent);
        }
        
    }

    /** Browse to the target URI using the Desktop.browse(URI)
    method.  For visual indication, change to the active color
    at method start, and return to the standard color once complete.
    This is usually so fast that the active color does not appear,
    but it will take longer if there is a problem finding/loading
    the browser or URI (e.g. for a File). */
    public void browse() {
        setForeground(activeColor);
        setBorder(activeBorder);
        try {
            Desktop.getDesktop().browse(getTarget());
        } catch(Exception e) {
            e.printStackTrace();
        }
        setForeground(standardColor);
        setBorder(standardBorder);
    }

    /** Browse to the target. */
    public void actionPerformed(ActionEvent ae) {
        browse();
    }

    /** Browse to the target. */
    public void mouseClicked(MouseEvent me) {
        browse();
    }

    /** Set the color to the hover color. */
    public void mouseEntered(MouseEvent me) {
        setForeground(hoverColor);
        setBorder(hoverBorder);
    }

    /** Set the color to the standard color. */
    public void mouseExited(MouseEvent me) {
        setForeground(standardColor);
        setBorder(standardBorder);
    }

    public void mouseReleased(MouseEvent me) {}

    public void mousePressed(MouseEvent me) {}

    /** Set the color to the standard color. */
    public void focusLost(FocusEvent fe) {
        setForeground(standardColor);
        setBorder(standardBorder);
    }

    /** Set the color to the hover color. */
    public void focusGained(FocusEvent fe) {
        setForeground(hoverColor);
        setBorder(hoverBorder);
    }

    public static void main(String[] args) throws Exception {
        JPanel p = new JPanel(new GridLayout(0,1));
        File f = new File(".","LinkLabel.java");

        /* Filename must be constructed with a canonical path in
        order to successfully use Desktop.browse(URI)! */
        f = new File(f.getCanonicalPath());

        URI uriFile = f.toURI();

        LinkLabel linkLabelFile = new LinkLabel(uriFile);
        linkLabelFile.init();
        p.add(linkLabelFile);

        LinkLabel linkLabelWeb = new LinkLabel(
            new URI("http://pscode.org/sscce.html"),
            "SSCCE");
        linkLabelWeb.setStandardColor(new Color(0,128,0));
        linkLabelWeb.setHoverColor(new Color(222,128,0));
        linkLabelWeb.init();

        /* This shows a quirk of the LinkLabel class, the
        size of the text field needs to be constrained to
        get the underline to appear properly. */
        p.add(linkLabelWeb);

        LinkLabel linkLabelConstrain = new LinkLabel(
            new URI("http://sdnshare.sun.com/"),
            "SDN Share");
        linkLabelConstrain.init();
        /* ..and this shows one way to constrain the size
        (appropriate for this layout).
        Similar tricks can be used to ensure the underline does
        not drop too far *below* the link (think BorderLayout
        NORTH/SOUTH).
        The same technique can also be nested further to produce
        a NORTH+EAST packing (for example). */
        JPanel labelConstrain = new JPanel(new BorderLayout());
        labelConstrain.add( linkLabelConstrain, BorderLayout.EAST );
        p.add(labelConstrain);

        LinkLabel linkLabelNoUnderline = new LinkLabel(
            new URI("http://java.net/"),
            "java.net");
        // another way to deal with the underline is to remove it
        linkLabelNoUnderline.setUnderlineVisible(false);
        // we can use the methods inherited from JTextField
        linkLabelNoUnderline.
            setHorizontalAlignment(JTextField.CENTER);
        linkLabelNoUnderline.init();
        p.add(linkLabelNoUnderline);

        JOptionPane.showMessageDialog( null, p );
    }
}
