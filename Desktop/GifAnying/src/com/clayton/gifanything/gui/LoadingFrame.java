package com.clayton.gifanything.gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class LoadingFrame extends JFrame {

	private JPanel contentPane;

	/**
	 * Create the frame.
	 */
	public LoadingFrame() {
		setTitle("Loading...");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 194, 81);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JLabel lblPleaseWaitLoading = new JLabel("Please wait, Preparing editor...");
		lblPleaseWaitLoading.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(lblPleaseWaitLoading, BorderLayout.CENTER);
		this.pack();
	}

}
