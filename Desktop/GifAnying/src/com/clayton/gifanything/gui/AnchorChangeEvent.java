package com.clayton.gifanything.gui;

import java.util.EventObject;

public class AnchorChangeEvent extends EventObject{
	private OverlayAnchor newAnchor;
	public AnchorChangeEvent(Object source, OverlayAnchor newAnc){
		super(source);
		this.newAnchor = newAnc;
	}
	public OverlayAnchor getNewAnchor(){ return this.newAnchor;}
}