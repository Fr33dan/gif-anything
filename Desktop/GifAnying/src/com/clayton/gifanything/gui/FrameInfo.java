package com.clayton.gifanything.gui;

import java.util.ArrayList;

public class FrameInfo{
	private final long originalDelay;
	private final int frame;
	private long delay;
	private boolean enabled = true;
	private FrameInfo affectedFrame;
	private ArrayList<FrameInfoChangeListener> listeners;
	FrameInfo(long d,int frame){
		this.delay = d;
		this.originalDelay = d;
		this.frame = frame;
		this.listeners = new ArrayList<FrameInfoChangeListener>();
	}
	private void fireChangeEvent(){
		final FrameInfoChangeEvent eventParam = new FrameInfoChangeEvent(this);
		for(FrameInfoChangeListener l : this.listeners){
			l.frameInfoChanged(eventParam);
		}
	}
	public void addChangeListener(FrameInfoChangeListener l){
		if(!this.listeners.contains(l))
			this.listeners.add(l);
	}
	public void removeChangeListener(FrameInfoChangeListener l){
		this.listeners.remove(l);
	}
	public void resetDelay(){
		if(this.delay!= this.originalDelay){
			this.delay = this.originalDelay;
			this.fireChangeEvent();
		}
	}
	public long getOriginalDelay() {
		return originalDelay;
	}
	public int getFrame() {
		return frame;
	}
	public long getDelay() {
		return delay;
	}
	public void setDelay(long delay) {
		if(this.delay!= delay){
			this.delay = delay;
			this.fireChangeEvent();
		}
	}
	public boolean isEnabled() {
		return enabled;
	}
	public void setEnabled(boolean enabled) {
		if(this.enabled!= enabled){
			this.enabled = enabled;
			this.fireChangeEvent();
		}
	}
	public FrameInfo getAffectedFrame() {
		return affectedFrame;
	}
	public void setAffectedFrame(FrameInfo affectedFrame) {
		this.affectedFrame = affectedFrame;
	}
	
	@Override
	public boolean equals(Object o){
		boolean returnVal;
		if(o instanceof FrameInfo){
			FrameInfo i = ((FrameInfo) o);
			returnVal = (i.delay == this.delay)
					 && (i.enabled == this.enabled)
					 && (i.originalDelay == this.originalDelay);
		} else{
			returnVal = false;
		}
		return returnVal;
	}
	
	
}