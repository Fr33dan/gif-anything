package com.clayton.gifanything.gui;

import java.awt.Component;
import java.awt.Font;

import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

	public class FontListCellRenderer implements ListCellRenderer<String>{
		private ListCellRenderer<Object> defaultRenderer;
		@SuppressWarnings("unchecked")
		public FontListCellRenderer(){
			this.defaultRenderer = (ListCellRenderer<Object>) new JComboBox<Object>().getRenderer();
		}
	
		@Override
		public Component getListCellRendererComponent(JList<? extends String> list,
				String value, int index, boolean isSelected, boolean cellHasFocus) {
			Component returnVal = this.defaultRenderer.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
			returnVal.setFont(new Font((String) value, returnVal.getFont().getStyle(), returnVal.getFont().getSize()));
			return returnVal;
		}
	}
