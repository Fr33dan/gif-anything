package com.clayton.gifanything.gui;

import java.util.EventListener;


public interface AnchorChangeListener extends EventListener{
	public void anchorChanged(AnchorChangeEvent event);
}