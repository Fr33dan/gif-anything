package com.clayton.gifanything.gui;

public enum OverlayAnchor { 
	NORTHEAST (true, true),
	SOUTHEAST (false, true),
	SOUTHWEST (false, false),
	NORTHWEST (true, false);
	private boolean north;
	private boolean east;
	OverlayAnchor(boolean north,boolean east){
		this.north = north;
		this.east = east;
	}
	
	public boolean isNorth(){ return north;}
	public boolean isEast(){ return east;}
}