package com.clayton.gifanything.gui;

import com.clayton.gifanything.Main;

class GifMakerThread extends Thread{
	/**
	 * 
	 */
	private final GifPanel gifPanel;

	/**
	 * @param gifPanel
	 */
	GifMakerThread(GifPanel gifPanel) {
		this.gifPanel = gifPanel;
	}

	public void run(){
		if(Main.log)
			System.err.println("Gif Creation Started");
		long startTime = System.currentTimeMillis();
		if(this.gifPanel.createGifInMemory()){
			long totalTime = System.currentTimeMillis() - startTime;
			if(Main.log)
				System.err.println("Gif Creation Time:" + totalTime);
		} else{
			if(Main.log)
				System.err.println("Gif Creation Canceled");
		}
	}
}