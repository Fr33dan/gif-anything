package com.clayton.gifanything.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.BufferedImage;
import java.beans.Beans;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream; 
import javax.imageio.stream.MemoryCacheImageInputStream;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;

import com.clayton.gifanything.Main;
import com.clayton.gifanything.MonitorScreen;
import com.clayton.gifanything.gifmaker.AnimatedGifEncoder;
import com.clayton.gifanything.gifmaker.GifProgressEvent;
import com.clayton.gifanything.gifmaker.GifProgressListener;

public class GifPanel extends JComponent implements ActionListener, MouseMotionListener,MouseListener, GifProgressListener, AncestorListener, WindowListener, FrameInfoChangeListener
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final String LOADING_STRING = "Loading GIF...";
	
	private Window parent;
	
	private SizeChangeListener sizeChangeListener;
	private final BufferedImage[] originalFrames;
	private final FrameInfo[] frameData;
	private BufferedImage[] gifFrames;
	private BufferedImage captionImage;
	
	private volatile boolean dragging;
	private volatile Point dragStart;
	private volatile Point dragEnd;
	private Rectangle cropRectangle;
	private double scalingFactor = 1;
	
	private OverlayAnchor overlayAnchor = OverlayAnchor.SOUTHEAST;
	private BufferedImage overlay;
	private double overlayScale = 1;
	
	private volatile String displayText = GifPanel.LOADING_STRING;
	
	private byte[] gifData;
	private int currentIndex;
	private int colorCount;
	private boolean globalColorTable = false;
	private Thread gifMakingThread;
	private Font font = new JLabel().getFont();
	private Timer changeImageTimer;
	
	private String caption;
	private Color captionColor = Color.WHITE;
	private Font captionFont = new Font("Arial", Font.BOLD, 22);
	private Color borderColor = Color.BLACK;
	private int borderDepth = 3;
	
	private boolean ditheringEnabled;
	
	private volatile boolean seekFileSize;
	private volatile long targetFileSize;
	
	public void seekTargetSize(long fileSize){
		this.targetFileSize = fileSize;
		this.seekFileSize = true;
		this.invalidateGif();
		this.invalidate();
	}
	
	//public GifPanel(){}
	public GifPanel(BufferedImage[] images, long delay){
		this.addMouseListener(this);
		this.addMouseMotionListener(this);
		this.addAncestorListener(this);
		FontMetrics m = this.getFontMetrics(this.font);
		new Dimension(m.stringWidth(LOADING_STRING),m.getHeight());
		this.originalFrames = images;
		this.colorCount = 256;
		
		if(this.originalFrames == null){
			this.frameData = new FrameInfo[0];
			if(!Beans.isDesignTime())
				throw new IllegalArgumentException("images must not be null");
		} else{
			this.frameData = new FrameInfo[this.originalFrames.length];
			for(int j = 0; j < this.frameData.length;j++){
				this.frameData[j] = new FrameInfo(delay,j);
				this.frameData[j].addChangeListener(this);
			}
			this.cropRectangle = new Rectangle(0, 0, images[0].getWidth(), images[0].getHeight());
			
			this.changeImageTimer = new Timer((int)delay,this);
			this.changeImageTimer.start();
		}
	}
	public int getFrameCount(){
		return this.frameData.length;
	}
	public BufferedImage[] getOriginalFrames() {
		return originalFrames;
	}
	
	public FrameInfo getFrameInfo(int frame){
		return this.frameData[frame];
	}
	
	private boolean hasCaption(){
		return this.caption!=null && !this.caption.equals("");
	}
	
	private void invalidateGif(){
		if(this.gifMakingThread!=null){
			this.gifMakingThread.interrupt();
		}
		this.gifData = null;
		this.gifFrames = null;
		this.gifMakingThread = null;
		this.repaint();
	}
	
	public void saveImage(String location) throws IOException{
		if(this.gifData == null && this.gifMakingThread!=null && !this.gifMakingThread.isAlive()){
			this.gifMakingThread = new GifMakerThread(this);
        	this.gifMakingThread.start();
		}
		while(this.gifMakingThread.isAlive()){
			try
			{
				Thread.sleep(10);
			}
			catch (InterruptedException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		FileOutputStream output = new FileOutputStream(location);
		output.write(this.gifData);
		output.flush();
		output.close();
		
	}
	
	public Color getBorderColor()
	{
		return borderColor;
	}
	public void setBorderColor(Color borderColor)
	{
		if(!this.borderColor.equals(borderColor)){
			this.borderColor = borderColor;
			
			if(this.hasCaption())
				this.invalidateGif();
		}
	}
	public int getBorderDepth()
	{
		return borderDepth;
	}
	public void setBorderDepth(int borderDepth)
	{
		if(this.borderDepth!=borderDepth){
			this.borderDepth = borderDepth;
	
			if(this.hasCaption())
				this.invalidateGif();
		}
	}
	public Font getCaptionFont()
	{
		return captionFont;
	}
	public void setCaptionFont(Font captionFont)
	{
		if(!this.captionFont.equals(captionFont)){
			this.captionFont = captionFont;
	
			if(this.hasCaption())
				this.invalidateGif();
		}
	}
	public Color getCaptionColor()
	{
		return captionColor;
	}
	public void setCaptionColor(Color captionColor)
	{
		if(!this.captionColor.equals(captionColor)){
			this.captionColor = captionColor;
	
			if(this.hasCaption())
				this.invalidateGif();
		}
	}
	public String getCaption(){
		return this.caption;
	}
	
	public void setCaption(String c){
		if(this.caption == null || !this.caption.equals(c)){
			this.caption = c;
		
			this.invalidateGif();
		}
	}
	
	
	public OverlayAnchor getOverlayAnchor() {
		return overlayAnchor;
	}
	public void setOverlayAnchor(OverlayAnchor overlayAnchor) {
		if(this.overlayAnchor != overlayAnchor){
			this.overlayAnchor = overlayAnchor;
			if(this.overlay!=null){
				this.invalidateGif();
			}
		}
	}
	public BufferedImage getOverlay() {
		return overlay;
	}
	public void setOverlay(BufferedImage overlay) {
		if(this.overlay!= overlay){
			this.overlay = overlay;
			this.invalidateGif();
		}
	}
	public double getOverlayScale() {
		return overlayScale;
	}
	public void setOverlayScale(double overlayScale) {
		if(this.overlayScale!= overlayScale){
			this.overlayScale = overlayScale;
			if(this.overlay!=null){
				this.invalidateGif();
			}
		}
	}
	
	public  Dimension getFinalSize(){
        return new Dimension((int)(this.cropRectangle.width * this.scalingFactor),(int)(this.cropRectangle.height * this.scalingFactor));
	}
	private Dimension getScaledSize(){
        return new Dimension((int)(this.originalFrames[0].getWidth() * this.scalingFactor),(int)(this.originalFrames[0].getHeight() * this.scalingFactor));
	}
	
	@Override
	public Dimension getPreferredSize() {
		if(this.originalFrames==null)
			return super.getPreferredSize();
		
        return this.getScaledSize();
    }
	
	@Override
	public Dimension getMinimumSize() {
		if(this.originalFrames==null)
			return super.getMinimumSize();
		
        return this.getScaledSize();
    }
	
	@Override
	public Dimension getMaximumSize() {
		if(this.originalFrames==null)
			return super.getMaximumSize();
		
        return this.getScaledSize();
    }
	public int getColorCount()
	{
		return colorCount;
	}
	public void setColorCount(int colorCount)
	{
		if(this.colorCount!=colorCount){
			this.colorCount = colorCount;
			this.invalidateGif();
		}
	}
	public boolean isDitheringEnabled() {
		return ditheringEnabled;
	}
	public void setDitheringEnabled(boolean ditheringEnabled) {
		if(this.ditheringEnabled != ditheringEnabled){
			this.ditheringEnabled = ditheringEnabled;
			this.invalidateGif();
		}
	}
	public boolean isGlobalColorTable()
	{
		return globalColorTable;
	}
	public void setGlobalColorTable(boolean globalColorTable)
	{
		if(this.globalColorTable!= globalColorTable){
			this.globalColorTable = globalColorTable;
			this.invalidateGif();
		}
	}
	public double getScalingFactor() {
		return scalingFactor;
	}
	public void setScalingFactor(double scalingFactor) {
		if(this.scalingFactor!= scalingFactor){
			this.scalingFactor = scalingFactor;
			this.invalidateGif();
			this.invalidate();
		}
	}
	public SizeChangeListener getSizeChangeListener()
	{
		return sizeChangeListener;
	}
	public void setSizeChangeListener(SizeChangeListener sizeChangeListener)
	{
		this.sizeChangeListener = sizeChangeListener;
	}
	
	private boolean scaleSize(double startingFactor,double increment){
		this.gifData = null;
		for(int j = 1; this.gifData == null || this.gifData.length < this.targetFileSize;j++){
			this.scalingFactor = startingFactor+  (j * increment);
			if(!this.createGifInMemory())
				return false;
		}
		this.scalingFactor = this.scalingFactor - increment;
		return true;
	}
	
	private boolean findGifScale(){
		boolean returnVal = this.scaleSize(0, .2);
		
		if(returnVal)
			returnVal = this.scaleSize(this.scalingFactor, .1);
		
		if(returnVal)
			returnVal = this.scaleSize(this.scalingFactor, .05);
		
		this.invalidateGif();
		this.invalidate();
		return true;
	}
	
	
	boolean createGifInMemory(){
		this.gifData = null;
		this.gifFrames = null;
		
		if(this.seekFileSize){
			this.seekFileSize = false;
			return findGifScale();
		}
		

		this.setDisplayText("Preparing Caption");
		if(this.caption == null || this.caption.isEmpty()){
			this.captionImage = null;
		} else{
			this.captionImage = this.createCaptionImage();
		}
		
		
		this.setDisplayText("Preparing GIF Encoder.");
		
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		AnimatedGifEncoder output = new AnimatedGifEncoder();
		output.addProgessListener(this);
		output.setRepeat(0);
		output.setColorCount(colorCount);
		output.setGlobalColorTable(globalColorTable);
		output.start(outputStream);
		output.setQuality(3);
		
		try{
			for(int j = 0; j < this.originalFrames.length;j++){
				if(this.frameData[j].isEnabled()){
					
					output.setDelay(this.frameData[j].getDelay());
					if(Thread.interrupted())
						return false;
					BufferedImage compiledImage = new BufferedImage(this.cropRectangle.width, this.cropRectangle.height, BufferedImage.TYPE_INT_ARGB);
					
					
					Graphics2D g = compiledImage.createGraphics();
					
					g.setRenderingHint(RenderingHints.KEY_DITHERING, this.isDitheringEnabled() ? RenderingHints.VALUE_DITHER_ENABLE:RenderingHints.VALUE_DITHER_DISABLE);
					g.drawImage(this.originalFrames[j]
								, 0
								, 0
								, this.cropRectangle.width
								, this.cropRectangle.height
								, this.cropRectangle.x
								, this.cropRectangle.y
								, this.cropRectangle.x + this.cropRectangle.width
								, this.cropRectangle.y + this.cropRectangle.height
								, null);
					
					if(captionImage!=null){
						g.drawImage(captionImage
								, 0
								, 0
								, null);
							//g.drawImage(this.originalFrames[j], 0, 0, null);
							//g.drawImage(this.captionImage, 0, 0, null);
					}
					
					if(this.overlay != null){
						Image overlayImage = this.overlay;
						
						// Not too worried about the overhead of this multiply by one in the case of no scaling
						int scaledHeight = (int)(this.overlay.getHeight() * this.overlayScale);
						int scaledWidth = (int)(this.overlay.getWidth() * this.overlayScale);
						// But scaling an image takes time so we should avoid it if we don't need to do it.
						if(this.overlayScale != 1)
							overlayImage = this.overlay.getScaledInstance(scaledWidth, scaledHeight, BufferedImage.SCALE_SMOOTH);
						
						int y;
						if(this.overlayAnchor.isNorth()){
							y = 0;
						} else {
							y = compiledImage.getHeight() - scaledHeight;
						}
	
						int x;
						if(this.overlayAnchor.isEast()){
							x = compiledImage.getWidth() - scaledWidth;
						} else {
							x = 0;
						}
						
						g.drawImage(overlayImage, x, y, null);
					}
					
					if(this.scalingFactor != 1){
						compiledImage = MonitorScreen.toBufferedImage(compiledImage.getScaledInstance( (int)(compiledImage.getWidth() * this.scalingFactor), (int)(compiledImage.getHeight() * this.scalingFactor), BufferedImage.SCALE_SMOOTH));
					}
					output.addFrame(compiledImage);
				}
			}
			boolean failed = !output.finish();
			if(failed)
				return false;
		}catch(InterruptedException e){
			return false;
		}
		

		if(Thread.interrupted())
			return false;
		byte[] newGifData = outputStream.toByteArray();
		ImageInputStream inputStream = new MemoryCacheImageInputStream(new ByteArrayInputStream(newGifData));
		
		ImageReader input = ImageIO.getImageReaders(inputStream).next();
		
		input.setInput(inputStream);
		ArrayList<BufferedImage> frames = new ArrayList<BufferedImage>();
		Iterator<IIOImage> images;
		try
		{
			images = input.readAll(null);
			while(images.hasNext()){
				if(Thread.interrupted())
					return false;
				frames.add((BufferedImage) images.next().getRenderedImage());
			}

			
			if(Thread.interrupted())
				return false;
			
			this.gifData = newGifData;
			this.gifFrames = frames.toArray(new BufferedImage[frames.size()]);
			
			if(this.sizeChangeListener !=null){
				this.sizeChangeListener.SizeChanged(this.gifData.length, this.getFinalSize());
			}
		}
		catch (IOException e)
		{
			this.setDisplayText("Unable to load GIF. Please restart GifAnything.");
		}
		return true;
	}
	
	private BufferedImage createCaptionImage(){
		BufferedImage returnVal = new BufferedImage(this.cropRectangle.width, this.cropRectangle.height, BufferedImage.TYPE_INT_ARGB);
		
		Graphics2D g = returnVal.createGraphics();
		
        g.setFont(this.getCaptionFont());
        g.setColor(this.captionColor);
        g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON );
       
        String[] lines = this.getCaption().split("\n");
        for(int j = 0; j < lines.length; j ++){
        	int width = g.getFontMetrics().stringWidth(lines[j]);
            int height = g.getFontMetrics().getHeight();
            
            int margin = (returnVal.getWidth() - width) / 2;
            
            int x = margin;
            int upFromBottom = lines.length - j - 1;
            int y = returnVal.getHeight() - (int)(height * .5 + (height * upFromBottom));
            
            g.drawString(lines[j], x, y);
        }
        
        
		
		return applyBorder(returnVal, this.getBorderDepth());
	}
	
	public void resetCropping(){
		this.cropRectangle = new Rectangle(0, 0, this.originalFrames[0].getWidth(), this.originalFrames[0].getHeight());
		this.invalidateGif();
	}
	
	private String getDisplayText() {
		return displayText;
	}
	private void setDisplayText(String displayText) {
		this.displayText = displayText;
		this.repaint();
	}
	public static boolean isAlpha(int sRGB)
	{
	    return (sRGB & 0xFF000000) > 0x01000000;
	}
	
	private BufferedImage applyBorder(BufferedImage source,int depth)
	{
		int sourceWidth = source.getWidth();
		int[] sourceImageData = source.getRGB(0, 0, sourceWidth, source.getHeight(), null, 0, sourceWidth);
		
		BufferedImage returnVal = new BufferedImage(sourceWidth,source.getHeight(),BufferedImage.TYPE_INT_ARGB);

		Graphics2D g = returnVal.createGraphics();
		g.setColor(this.getBorderColor());
		g.setRenderingHint(
		        RenderingHints.KEY_ANTIALIASING,
		        RenderingHints.VALUE_ANTIALIAS_ON);
		
		for(int j = 0;j < sourceImageData.length;j++)
		{	
			if(sourceImageData[j] != 0)
			{	
				int pixelX = j % sourceWidth;
				int pixelY = (int) (j / sourceWidth);
				
				g.fillOval(pixelX - depth, pixelY - depth, depth * 2 + 1, depth * 2 + 1);
			}
		}
		g.drawImage(source, 0, 0, null);
		
		return returnVal;
	}

	
	 @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        if(this.originalFrames==null)
        	return;
        
        Image originalFrame = this.originalFrames[this.currentIndex];
        if(this.gifFrames!=null || dragging){
        	
        	int orginalFrameWidth = originalFrame.getWidth(null);
			int originalFrameHeight = originalFrame.getHeight(null);
			if(this.scalingFactor != 1){
				orginalFrameWidth = (int)(orginalFrameWidth * this.scalingFactor);
				originalFrameHeight = (int)(originalFrameHeight * this.scalingFactor);
        		originalFrame = originalFrame.getScaledInstance(orginalFrameWidth, originalFrameHeight, BufferedImage.SCALE_SMOOTH);
        	}
        	g.drawImage(originalFrame,0,0,null);
        	
        	
        	Color bgColor = new Color(Color.BLACK.getRed(), Color.BLACK.getBlue(), Color.BLACK.getGreen(), 160);
    		g.setColor(bgColor);
    		
        	if(dragging){
        		int upperX = Math.min(this.dragStart.x, this.dragEnd.x);
        		int upperY = Math.min(this.dragStart.y, this.dragEnd.y);
        		
        		int lowerX = Math.max(this.dragStart.x, this.dragEnd.x);
        		int lowerY = Math.max(this.dragStart.y, this.dragEnd.y);
        		
        		g.fillRect(0, 0, orginalFrameWidth, upperY);
        		g.fillRect(0, upperY, upperX, originalFrameHeight - upperY);
        		g.fillRect(upperX, lowerY, orginalFrameWidth - upperX, originalFrameHeight - lowerY);
        		g.fillRect(lowerX, upperY, orginalFrameWidth - lowerX, originalFrameHeight - upperY - (originalFrameHeight - lowerY));
        		
        	} else{
        		g.fillRect(0, 0, orginalFrameWidth, originalFrameHeight);
            	
        		g.drawImage(this.gifFrames[getGifIndex(currentIndex)], (int)(this.cropRectangle.x * this.scalingFactor), (int)(this.cropRectangle.y * this.scalingFactor), null);
        	}
	        
        } else{
        	int x = (this.getWidth() - g.getFontMetrics().stringWidth(this.getDisplayText())) / 2;
        	int y = (this.getHeight() - g.getFontMetrics().getHeight()) / 2;
        	g.drawString(this.displayText, x, y);
        	if(this.gifMakingThread == null || !this.gifMakingThread.isAlive()){
	        	this.gifMakingThread = new GifMakerThread(this);
	        	this.gifMakingThread.start();
        	}
        }
    }
	 
	 private int getGifIndex(int originalIndex){
		 int disabledCount = 0;
		 for(int j = 0;j < originalIndex;j++){
			 if(!this.frameData[j].isEnabled())
				 disabledCount++;
		 }
		 
		 return originalIndex - disabledCount;
	 }
	 
	@Override
	public void actionPerformed(ActionEvent arg0)
	{
		if(this.gifFrames!=null){
			do{
				this.currentIndex++;
				if(this.currentIndex >= this.originalFrames.length)
					this.currentIndex = 0;
			} while(!this.frameData[this.currentIndex].isEnabled());
			
			this.changeImageTimer.setDelay((int) this.frameData[this.currentIndex].getDelay());
			this.repaint();
		}
	}
	@Override
	public void mouseDragged(MouseEvent arg0)
	{
		this.dragging = true;
		this.dragEnd = arg0.getPoint();
		this.repaint();
		
	}
	@Override
	public void mouseMoved(MouseEvent arg0)
	{
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseClicked(MouseEvent arg0)
	{
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseEntered(MouseEvent arg0)
	{
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseExited(MouseEvent arg0)
	{
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mousePressed(MouseEvent arg0)
	{
		this.dragStart = arg0.getPoint();
		
	}
	@Override
	public void mouseReleased(MouseEvent arg0)
	{
		this.dragging = false;
		this.dragEnd = arg0.getPoint();

		int upperX = (int) (Math.min(this.dragStart.x, this.dragEnd.x) / this.scalingFactor);
		int upperY = (int) (Math.min(this.dragStart.y, this.dragEnd.y) / this.scalingFactor);
		
		int lowerX = (int) (Math.max(this.dragStart.x, this.dragEnd.x) / this.scalingFactor);
		int lowerY = (int) (Math.max(this.dragStart.y, this.dragEnd.y) / this.scalingFactor);
		
		this.cropRectangle = new Rectangle(upperX,upperY, lowerX - upperX, lowerY - upperY);
		this.invalidateGif();
	}
	@Override
	public void onGifProgress(GifProgressEvent args) {
		if(Main.log)
			System.err.println(System.currentTimeMillis() + ":" + args.getProgressString() + "|" + args.getCompletionPercentage());
		this.setDisplayText(args.getProgressString());
		
	}
	@Override
	public void ancestorAdded(AncestorEvent arg0) {
		Window w = SwingUtilities.windowForComponent(this);
		if(w!=null){
			if(this.parent!=null){
				this.parent.removeWindowListener(this);
			}
			this.parent = w;
			this.parent.addWindowListener(this);
		}
		
	}
	@Override
	public void ancestorMoved(AncestorEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void ancestorRemoved(AncestorEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void windowActivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void windowClosed(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void windowClosing(WindowEvent arg0) {
		//this.changeImageTimer.stop();
		if(this.gifMakingThread!=null){
			this.gifMakingThread.interrupt();
		}
		
	}
	@Override
	public void windowDeactivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void windowDeiconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void windowIconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void windowOpened(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void frameInfoChanged(FrameInfoChangeEvent info) {
		this.invalidateGif();
		
	}
}
