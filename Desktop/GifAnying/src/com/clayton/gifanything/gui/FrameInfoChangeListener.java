package com.clayton.gifanything.gui;

import java.util.EventListener;

public interface FrameInfoChangeListener extends EventListener {
	public void frameInfoChanged(FrameInfoChangeEvent info);

}
