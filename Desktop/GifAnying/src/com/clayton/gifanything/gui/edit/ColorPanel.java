package com.clayton.gifanything.gui.edit;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import com.clayton.gifanything.gui.GifPanel;

public class ColorPanel extends JPanel {

	private GifPanel gifPanel;

	/**
	 * Create the panel.
	 */
	public ColorPanel(GifPanel panel) {
		gifPanel = panel;
		GridBagLayout gbl_colorPanel = new GridBagLayout();
		gbl_colorPanel.columnWidths = new int[]{0, 0, 0};
		gbl_colorPanel.rowHeights = new int[] {0, 0, 0, 0};
		gbl_colorPanel.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		gbl_colorPanel.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		this.setLayout(gbl_colorPanel);
		
		final JCheckBox chckbxGlobalColorTable = new JCheckBox("Global Color Table:");
		GridBagConstraints gbc_chckbxGlobalColorTable = new GridBagConstraints();
		gbc_chckbxGlobalColorTable.insets = new Insets(0, 0, 5, 0);
		gbc_chckbxGlobalColorTable.anchor = GridBagConstraints.EAST;
		gbc_chckbxGlobalColorTable.gridx = 1;
		gbc_chckbxGlobalColorTable.gridy = 0;
		this.add(chckbxGlobalColorTable, gbc_chckbxGlobalColorTable);
		chckbxGlobalColorTable.setToolTipText(EditFrame.TOOLTIP_GCT);
		chckbxGlobalColorTable.setHorizontalTextPosition(SwingConstants.LEFT);
		
		JLabel lblColorCount = new JLabel("Color Count:");
		GridBagConstraints gbc_lblColorCount = new GridBagConstraints();
		gbc_lblColorCount.insets = new Insets(0, 0, 0, 5);
		gbc_lblColorCount.gridx = 0;
		gbc_lblColorCount.gridy = 1;
		this.add(lblColorCount, gbc_lblColorCount);
		lblColorCount.setToolTipText(EditFrame.TOOLTIP_COLOR_COUNT);
		
		final JComboBox<Integer> comboBox = new JComboBox<Integer>();
		GridBagConstraints gbc_comboBox = new GridBagConstraints();
		gbc_comboBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBox.gridx = 1;
		gbc_comboBox.gridy = 1;
		this.add(comboBox, gbc_comboBox);
		comboBox.setToolTipText(EditFrame.TOOLTIP_COLOR_COUNT);
		comboBox.setModel(new DefaultComboBoxModel<Integer>(new Integer[] {256, 128, 64, 32}));
		
		final JCheckBox chckbxDithering = new JCheckBox("Dithering:");
		chckbxDithering.setVisible(false);
		GridBagConstraints gbc_chckbxDithering = new GridBagConstraints();
		gbc_chckbxDithering.insets = new Insets(0, 0, 5, 0);
		gbc_chckbxDithering.anchor = GridBagConstraints.EAST;
		gbc_chckbxDithering.gridx = 1;
		gbc_chckbxDithering.gridy = 2;
		this.add(chckbxDithering, gbc_chckbxDithering);
		chckbxDithering.setHorizontalTextPosition(SwingConstants.LEFT);
		

		comboBox.addPropertyChangeListener(new PropertyChangeListener() {
			public void propertyChange(PropertyChangeEvent arg0) {
				ColorPanel.this.gifPanel.setColorCount((Integer)comboBox.getSelectedItem());
			}
		});
		chckbxGlobalColorTable.addPropertyChangeListener(new PropertyChangeListener() {
			public void propertyChange(PropertyChangeEvent arg0) {
				ColorPanel.this.gifPanel.setGlobalColorTable(chckbxGlobalColorTable.isSelected());
			}
		});
		chckbxDithering.addPropertyChangeListener(new PropertyChangeListener() {
			public void propertyChange(PropertyChangeEvent arg0) {
				ColorPanel.this.gifPanel.setDitheringEnabled(chckbxDithering.isSelected());
			}
		});
	}

}
