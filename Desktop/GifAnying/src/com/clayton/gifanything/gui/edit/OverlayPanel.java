package com.clayton.gifanything.gui.edit;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.prefs.Preferences;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.BadLocationException;

import com.clayton.gifanything.gui.AnchorChangeEvent;
import com.clayton.gifanything.gui.AnchorChangeListener;
import com.clayton.gifanything.gui.GifPanel;
import com.clayton.gifanything.gui.OverlayAnchorPanel;

public class OverlayPanel extends JPanel {

	private GifPanel gifPanel;
	private JTextField overlayScalingField;
	private JSlider overlayScalingSlider;

	/**
	 * Create the panel.
	 */
	public OverlayPanel(GifPanel panel) {
		gifPanel = panel;
		
		GridBagLayout gbl_overlayPanel = new GridBagLayout();
		gbl_overlayPanel.columnWidths = new int[]{0, 0, 0, 0};
		gbl_overlayPanel.rowHeights = new int[]{0, 0, 0, 0, 0};
		gbl_overlayPanel.columnWeights = new double[]{1.0, 1.0, 0.0, Double.MIN_VALUE};
		gbl_overlayPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		this.setLayout(gbl_overlayPanel);
		
		JButton btnSelectOverlayImage = new JButton("Select Overlay Image");
		btnSelectOverlayImage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String[] fileTypes = ImageIO.getReaderFileSuffixes();
				
				JFileChooser fileOpenDialog = new JFileChooser();
				fileOpenDialog.setFileFilter(new FileNameExtensionFilter("Image files", fileTypes));
				if(fileOpenDialog.showOpenDialog(OverlayPanel.this) == JFileChooser.APPROVE_OPTION){
					try {
						gifPanel.setOverlay(ImageIO.read(fileOpenDialog.getSelectedFile()));
					} catch (IOException e) {
						JOptionPane.showMessageDialog(OverlayPanel.this, "Could not open file\n" + e.getMessage(), "Could not open overlay image", JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		});
		btnSelectOverlayImage.setToolTipText(EditFrame.TOOLTIP_OVERLAY_ADD);
		GridBagConstraints gbc_btnSelectOverlayImage = new GridBagConstraints();
		gbc_btnSelectOverlayImage.anchor = GridBagConstraints.EAST;
		gbc_btnSelectOverlayImage.gridwidth = 2;
		gbc_btnSelectOverlayImage.insets = new Insets(0, 0, 5, 0);
		gbc_btnSelectOverlayImage.gridx = 1;
		gbc_btnSelectOverlayImage.gridy = 0;
		this.add(btnSelectOverlayImage, gbc_btnSelectOverlayImage);
		
		JButton btnRemoveOverlay = new JButton("Remove Overlay");
		btnRemoveOverlay.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				gifPanel.setOverlay(null);
			}
		});
		btnRemoveOverlay.setToolTipText(EditFrame.TOOLTIP_OVERLAY_REMOVE);
		GridBagConstraints gbc_btnRemoveOverlay = new GridBagConstraints();
		gbc_btnRemoveOverlay.anchor = GridBagConstraints.EAST;
		gbc_btnRemoveOverlay.gridwidth = 2;
		gbc_btnRemoveOverlay.insets = new Insets(0, 0, 5, 0);
		gbc_btnRemoveOverlay.gridx = 1;
		gbc_btnRemoveOverlay.gridy = 1;
		this.add(btnRemoveOverlay, gbc_btnRemoveOverlay);
		
		overlayScalingField = new JTextField();
		overlayScalingField.setToolTipText(EditFrame.TOOLTIP_OVERLAY_SCALING);
		overlayScalingField.setText("100%");
		overlayScalingField.setColumns(4);
		overlayScalingField.getDocument().addDocumentListener(new DocumentListener(){

			public void update(DocumentEvent arg0){
				try
				{
					String newValue = arg0.getDocument().getText(0, arg0.getDocument().getLength());
					if(!newValue.equals("") && gifPanel != null && !(overlayScalingSlider!=null && overlayScalingSlider.getValueIsAdjusting())){
						if(newValue.endsWith("%"))
							newValue = newValue.substring(0, newValue.length() - 1);
						
						double newScalingValue = Double.parseDouble(newValue)/100;
						gifPanel.setOverlayScale(newScalingValue);
					}
				}
				catch (BadLocationException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			@Override
			public void changedUpdate(DocumentEvent arg0)
			{
				update(arg0);
			}

			@Override
			public void insertUpdate(DocumentEvent arg0)
			{	
				update(arg0);
			}

			@Override
			public void removeUpdate(DocumentEvent arg0)
			{	
				update(arg0);
			}
			
		});
		
		JLabel lblScale = new JLabel("Scaling");
		lblScale.setToolTipText(EditFrame.TOOLTIP_OVERLAY_SCALING);
		GridBagConstraints gbc_lblScale = new GridBagConstraints();
		gbc_lblScale.insets = new Insets(0, 0, 5, 5);
		gbc_lblScale.gridx = 0;
		gbc_lblScale.gridy = 2;
		this.add(lblScale, gbc_lblScale);
		GridBagConstraints gbc_overlayScalingField = new GridBagConstraints();
		gbc_overlayScalingField.insets = new Insets(0, 0, 5, 0);
		gbc_overlayScalingField.fill = GridBagConstraints.HORIZONTAL;
		gbc_overlayScalingField.gridx = 2;
		gbc_overlayScalingField.gridy = 2;
		this.add(overlayScalingField, gbc_overlayScalingField);
		
		overlayScalingSlider = new JSlider();
		overlayScalingSlider.setToolTipText(EditFrame.TOOLTIP_OVERLAY_SCALING);
		overlayScalingSlider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				overlayScalingField.setText(overlayScalingSlider.getValue() + "%");
			}
		});
		GridBagConstraints gbc_overlayScalingSlider = new GridBagConstraints();
		gbc_overlayScalingSlider.insets = new Insets(0, 0, 5, 5);
		gbc_overlayScalingSlider.gridx = 1;
		gbc_overlayScalingSlider.gridy = 2;
		this.add(overlayScalingSlider, gbc_overlayScalingSlider);
		overlayScalingSlider.setValue(100);
		
		OverlayAnchorPanel anchorPanel = new OverlayAnchorPanel();
		anchorPanel.setAnchorListener(new AnchorChangeListener(){

			@Override
			public void anchorChanged(AnchorChangeEvent event) {
				gifPanel.setOverlayAnchor(event.getNewAnchor());
			}
			
		});
		anchorPanel.setToolTipText(EditFrame.TOOLTIP_OVERLAY_ANCHOR);
		GridBagConstraints gbc_anchorPanel = new GridBagConstraints();
		gbc_anchorPanel.anchor = GridBagConstraints.NORTHEAST;
		gbc_anchorPanel.gridx = 2;
		gbc_anchorPanel.gridy = 3;
		this.add(anchorPanel, gbc_anchorPanel);

	}

}
