package com.clayton.gifanything.gui.edit;

import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.beans.Beans;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.prefs.Preferences;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;

import com.clayton.gifanything.Main;
import com.clayton.gifanything.gui.FontListCellRenderer;
import com.clayton.gifanything.gui.GifPanel;

public class CaptionPanel extends JPanel {
	private GifPanel gifPanel;
	private Preferences savedPrefs;
	private JCheckBox chckbxItalic;
	private JCheckBox chckbxBold;
	/**
	 * Create the panel.
	 */
	public CaptionPanel(GifPanel editedPanel,Preferences prefs) {
		this.gifPanel = editedPanel;
		this.savedPrefs = prefs;
		GridBagLayout gbl_captionPanel = new GridBagLayout();
		gbl_captionPanel.columnWidths = new int[] {0, 0};
		gbl_captionPanel.rowHeights = new int[] {0, 0, 0, 0, 0};
		gbl_captionPanel.columnWeights = new double[]{1.0, 1.0};
		gbl_captionPanel.rowWeights = new double[]{1.0, 0.0, 0.0, 0.0, 0.0};
		this.setLayout(gbl_captionPanel);
		
		JLabel lblCaption = new JLabel("Caption:");
		GridBagConstraints gbc_lblCaption = new GridBagConstraints();
		gbc_lblCaption.anchor = GridBagConstraints.EAST;
		gbc_lblCaption.insets = new Insets(5, 5, 5, 0);
		gbc_lblCaption.gridx = 0;
		gbc_lblCaption.gridy = 0;
		this.add(lblCaption, gbc_lblCaption);
		lblCaption.setToolTipText(EditFrame.TOOLTIP_CAPTION_INPUT);
		
		JTextArea captionArea = new JTextArea();
		GridBagConstraints gbc_captionArea = new GridBagConstraints();
		gbc_captionArea.insets = new Insets(5, 0, 5, 0);
		gbc_captionArea.anchor = GridBagConstraints.WEST;
		gbc_captionArea.fill = GridBagConstraints.BOTH;
		gbc_captionArea.gridx = 1;
		gbc_captionArea.gridy = 0;
		this.add(captionArea, gbc_captionArea);
		captionArea.setToolTipText(EditFrame.TOOLTIP_CAPTION_INPUT);
		captionArea.getDocument().addDocumentListener(new DocumentListener(){

			public void update(DocumentEvent arg0){
				try
				{
					String newCaption = arg0.getDocument().getText(0, arg0.getDocument().getLength());
					gifPanel.setCaption(newCaption);
				}
				catch (BadLocationException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			@Override
			public void changedUpdate(DocumentEvent arg0)
			{
				update(arg0);
			}

			@Override
			public void insertUpdate(DocumentEvent arg0)
			{	
				update(arg0);
			}

			@Override
			public void removeUpdate(DocumentEvent arg0)
			{	
				update(arg0);
			}
			
		});
		
		JButton btnChangeTextColor = new JButton("Change Text Color");
		btnChangeTextColor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				gifPanel.setCaptionColor(JColorChooser.showDialog(
	                     CaptionPanel.this,
	                     "Choose Border Color",
	                     gifPanel.getCaptionColor()));
			}
		});
		GridBagConstraints gbc_btnChangeTextColor = new GridBagConstraints();
		gbc_btnChangeTextColor.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnChangeTextColor.insets = new Insets(0, 0, 5, 0);
		gbc_btnChangeTextColor.gridx = 1;
		gbc_btnChangeTextColor.gridy = 1;
		this.add(btnChangeTextColor, gbc_btnChangeTextColor);
		btnChangeTextColor.setToolTipText(EditFrame.TOOLTIP_CAPTION_COLOR);
		
		JPanel panel = new JPanel();
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.anchor = GridBagConstraints.NORTH;
		gbc_panel.gridwidth = 2;
		gbc_panel.insets = new Insets(0, 5, 5, 0);
		gbc_panel.fill = GridBagConstraints.HORIZONTAL;
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 2;
		this.add(panel, gbc_panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0, 0, 0, 0, 0, 0};
		gbl_panel.rowHeights = new int[] {0, 0};
		gbl_panel.columnWeights = new double[]{0.0, 0.0, 1.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 0.0};
		panel.setLayout(gbl_panel);
		
		JLabel lblFont = new JLabel("Font:");
		lblFont.setToolTipText(EditFrame.TOOLTIP_FONT_CHANGE);
		GridBagConstraints gbc_lblFont = new GridBagConstraints();
		gbc_lblFont.anchor = GridBagConstraints.EAST;
		gbc_lblFont.insets = new Insets(0, 0, 5, 5);
		gbc_lblFont.gridx = 0;
		gbc_lblFont.gridy = 0;
		panel.add(lblFont, gbc_lblFont);
		
		
		JComboBox<String> fontComboBox = new JComboBox<String>();

		fontComboBox.setToolTipText(EditFrame.TOOLTIP_FONT_CHANGE);
		fontComboBox.setModel(new DefaultComboBoxModel<String>(GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames()));


		if(!Beans.isDesignTime()){
			String font =this.savedPrefs.get(Main.DEFAULT_CAPTION_FONT_NAME, "Arial");
			fontComboBox.setSelectedItem(font);
		}
		
		fontComboBox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
			       if (arg0.getStateChange() == ItemEvent.SELECTED) {
			    	   Font oldFont = gifPanel.getCaptionFont();
			    	   Font newFont = new Font((String) arg0.getItem(), oldFont.getStyle(), oldFont.getSize());
			    	   gifPanel.setCaptionFont(newFont);
			       }
			}
		});
		
		fontComboBox.setRenderer(new FontListCellRenderer());
		

		GridBagConstraints gbc_fontComboBox = new GridBagConstraints();
		gbc_fontComboBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_fontComboBox.gridwidth = 4;
		gbc_fontComboBox.insets = new Insets(0, 0, 5, 0);
		gbc_fontComboBox.gridx = 1;
		gbc_fontComboBox.gridy = 0;
		panel.add(fontComboBox, gbc_fontComboBox);
		
		JLabel label_1 = new JLabel("Size:");
		label_1.setToolTipText(EditFrame.TOOLTIP_FONT_SIZE);
		GridBagConstraints gbc_label_1 = new GridBagConstraints();
		gbc_label_1.anchor = GridBagConstraints.EAST;
		gbc_label_1.insets = new Insets(0, 0, 5, 5);
		gbc_label_1.gridx = 1;
		gbc_label_1.gridy = 1;
		panel.add(label_1, gbc_label_1);
		
		final JSpinner fontSizeSpinner = new JSpinner();
		if(!Beans.isDesignTime())
			fontSizeSpinner.setModel(new SpinnerNumberModel(new Integer(savedPrefs.getInt(Main.DEFAULT_CAPTION_FONT_SIZE, 12)), new Integer(5), null, new Integer(1)));
		fontSizeSpinner.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				Integer newFontSize = (Integer) fontSizeSpinner.getValue();

	    	   Font oldFont = gifPanel.getCaptionFont();
	    	   Font newFont = new Font(oldFont.getName(), oldFont.getStyle(), newFontSize);
	    	   gifPanel.setCaptionFont(newFont);
			}
		});
		fontSizeSpinner.setToolTipText(EditFrame.TOOLTIP_FONT_SIZE);
		GridBagConstraints gbc_fontSizeSpinner = new GridBagConstraints();
		gbc_fontSizeSpinner.fill = GridBagConstraints.HORIZONTAL;
		gbc_fontSizeSpinner.insets = new Insets(0, 0, 5, 5);
		gbc_fontSizeSpinner.gridx = 2;
		gbc_fontSizeSpinner.gridy = 1;
		panel.add(fontSizeSpinner, gbc_fontSizeSpinner);
		
		PropertyChangeListener styleChangeListener = new PropertyChangeListener() {
			public void propertyChange(PropertyChangeEvent arg0) {
				int style = Font.PLAIN;
				
				if(chckbxItalic.isSelected())
					style |= Font.ITALIC;
				
				if(chckbxBold.isSelected())
					style |= Font.BOLD;
				
	    	   Font oldFont = gifPanel.getCaptionFont();
	    	   Font newFont = new Font(oldFont.getName(), style, oldFont.getSize());
	    	   gifPanel.setCaptionFont(newFont);
			}
		};

		
		int defaultStyle = 0;
		if(!Beans.isDesignTime())
			defaultStyle = savedPrefs.getInt(Main.DEFAULT_CAPTION_FONT_STYLE, Font.PLAIN);
		
		chckbxItalic = new JCheckBox("Italic");
		chckbxItalic.setSelected((defaultStyle & Font.ITALIC) != 0);
		chckbxItalic.setToolTipText(EditFrame.TOOLTIP_FONT_STYLE);
		GridBagConstraints gbc_chckbxItalic = new GridBagConstraints();
		gbc_chckbxItalic.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxItalic.gridx = 3;
		gbc_chckbxItalic.gridy = 1;
		if(!Beans.isDesignTime())
			chckbxItalic.addPropertyChangeListener("BUTTON.BP_CHECKBOX", styleChangeListener);
		panel.add(chckbxItalic, gbc_chckbxItalic);
		
		chckbxBold = new JCheckBox("Bold");
		chckbxBold.setSelected((defaultStyle & Font.BOLD) != 0);
		chckbxBold.setToolTipText(EditFrame.TOOLTIP_FONT_STYLE);
		GridBagConstraints gbc_chckbxBold = new GridBagConstraints();
		gbc_chckbxBold.insets = new Insets(0, 0, 5, 0);
		gbc_chckbxBold.gridx = 4;
		gbc_chckbxBold.gridy = 1;
		if(!Beans.isDesignTime())
			chckbxBold.addPropertyChangeListener("BUTTON.BP_CHECKBOX", styleChangeListener);
		panel.add(chckbxBold, gbc_chckbxBold);
		
		JButton btnChangeBorderColor = new JButton("Change Border Color");
		GridBagConstraints gbc_btnChangeBorderColor = new GridBagConstraints();
		gbc_btnChangeBorderColor.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnChangeBorderColor.insets = new Insets(0, 0, 5, 0);
		gbc_btnChangeBorderColor.gridx = 1;
		gbc_btnChangeBorderColor.gridy = 3;
		this.add(btnChangeBorderColor, gbc_btnChangeBorderColor);
		btnChangeBorderColor.setToolTipText(EditFrame.TOOLTIP_BORDER_COLOR);
		btnChangeBorderColor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				gifPanel.setBorderColor(JColorChooser.showDialog(
											                     CaptionPanel.this,
											                     "Choose Border Color",
											                     gifPanel.getBorderColor()));
			}
		});
		
		JLabel lblBorderDepth = new JLabel("Border Depth:");
		lblBorderDepth.setToolTipText(EditFrame.TOOLTIP_BORDER_DEPTH);
		GridBagConstraints gbc_lblBorderDepth = new GridBagConstraints();
		gbc_lblBorderDepth.insets = new Insets(0, 0, 5, 5);
		gbc_lblBorderDepth.gridx = 0;
		gbc_lblBorderDepth.gridy = 4;
		this.add(lblBorderDepth, gbc_lblBorderDepth);
		lblBorderDepth.setToolTipText(EditFrame.TOOLTIP_BORDER_DEPTH);
		
		final JSpinner borderSpinner = new JSpinner();
		GridBagConstraints gbc_borderSpinner = new GridBagConstraints();
		gbc_borderSpinner.insets = new Insets(0, 0, 5, 0);
		gbc_borderSpinner.fill = GridBagConstraints.HORIZONTAL;
		gbc_borderSpinner.gridx = 1;
		gbc_borderSpinner.gridy = 4;
		this.add(borderSpinner, gbc_borderSpinner);
		borderSpinner.setToolTipText(EditFrame.TOOLTIP_BORDER_DEPTH);
		if(!Beans.isDesignTime())
			borderSpinner.setModel(new SpinnerNumberModel(savedPrefs.getInt(Main.DEFAULT_BORDER_DEPTH, 0), 0, 15, 1));
		
		borderSpinner.addChangeListener(new ChangeListener() {

		    @Override
		    public void stateChanged(ChangeEvent e) {
		    	gifPanel.setBorderDepth((int)borderSpinner.getValue());
		    }
		});

	}

}
