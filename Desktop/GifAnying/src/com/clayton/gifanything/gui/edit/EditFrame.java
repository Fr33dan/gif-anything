package com.clayton.gifanything.gui.edit;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import javax.imageio.ImageIO;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.ToolTipManager;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultFormatter;

import com.clayton.gifanything.Main;

import javax.swing.border.TitledBorder;
import javax.swing.border.EtchedBorder;

import com.clayton.gifanything.gui.GifPanel;
import com.clayton.gifanything.gui.PreferencesFrame;
import com.clayton.gifanything.gui.SizeChangeListener;

import javax.swing.JSplitPane;

public class EditFrame extends JFrame
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String TOOLTIP_FILE_SIZE = "The file size of the resulting GIF.";
	public static final String TOOLTIP_FINAL_FRAME = "<HTML><p width=\"3in\">Indicates which of the frames recorded is the last frame to use in the final GIF.</p></HTML>";
	public static final String TOOLTIP_FIRST_FRAME = "<HTML><p width=\"3in\">Indicates which of the frames recorded is the first frame to use in the final GIF.</p></HTML>";
	public static final String TOOLTIP_COLOR_COUNT = "<HTML><p width=\"3in\">Sets the number of colors that will be used in the GIF. If less colors are used the file size is decreased, but so is the image quality.</p></HTML>";
	public static final String TOOLTIP_GCT = "<HTML><p width=\"3in\">If a global color table is used only one color table for all frames will have to be written to the file which will decrease size. Otherwise each frame will be given it's own color table.<br><br>If all the frames share the same colors this using a global color table will not generate a noticable change in quality.</p></HTML>";
	public static final String TOOLTIP_BORDER_DEPTH = "<HTML><p width=\"3in\">Indicates (in pixels) how far the border should reach beyond the text caption text.</p></HTML>";
	public static final String TOOLTIP_BORDER_COLOR = "<HTML><p width=\"3in\">Change the color of the caption's border.</p></HTML>";
	public static final String TOOLTIP_CAPTION_COLOR = "<HTML><p width=\"3in\">Change the text color of the caption.</p></HTML>";
	public static final String TOOLTIP_CAPTION_INPUT = "<HTML><p width=\"3in\">Input your caption to the image.</p></HTML>";
	public static final String TOOLTIP_FONT_CHANGE = "<HTML><p width=\"3in\">Choose the font to use for your caption</p></HTML>";
	public static final String TOOLTIP_FONT_SIZE = "<HTML><p width=\"3in\">Adjust the size of your caption.</p></HTML>";
	public static final String TOOLTIP_FONT_STYLE = "<HTML><p width=\"3in\">Change the font style of your caption.</p></HTML>";
	public static final String TOOLTIP_SCALING = "<HTML><p width=\"3in\">Adjust the size of the final GIF.</p></HTML>";
	public static final String TOOLTIP_OVERLAY_SCALING = "<HTML><p width=\"3in\">Adjust the size of the the overlay on the GIF.</p></HTML>";
	public static final String TOOLTIP_OVERLAY_REMOVE = "<HTML><p width=\"3in\">Remove the overlay from the GIF.</p></HTML>";
	public static final String TOOLTIP_OVERLAY_ADD = "<HTML><p width=\"3in\">Select an image file with transparency to overlay on the GIF.</p></HTML>";
	public static final String TOOLTIP_OVERLAY_ANCHOR = "<HTML><p width=\"3in\">Select which corner of the GIF the overlay should be placed in.</p></HTML>";
	private JPanel contentPane;
	private GifPanel gifPanel;
	private Preferences savedPrefs;
	private JLabel fileSizeLabel;
	private JLabel lblFinalDimensions;
	private ScalingPanel scalingAndAnimationPanel;
	
	private boolean notSaved = true;

	/**
	 * Create the frame.
	 */
	public EditFrame(final BufferedImage[] images, Preferences pref)
	{

		ToolTipManager.sharedInstance().setDismissDelay(30000);
		setIconImage(Main.Window_Icon);
	
		this.savedPrefs = pref;
		
		setTitle("Edit Gif");
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 522, 436);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JSplitPane splitPane = new JSplitPane();
		contentPane.add(splitPane, BorderLayout.CENTER);
		
		JScrollPane gifScrollPane = new JScrollPane();
		splitPane.setLeftComponent(gifScrollPane);
		
		JPanel gifContainer = new JPanel();
		gifContainer.setToolTipText("Click and drag to crop.");
		gifScrollPane.setViewportView(gifContainer);
		GridBagLayout gbl_gifContainer = new GridBagLayout();
		gifContainer.setLayout(gbl_gifContainer);
		gifPanel = new GifPanel(images, savedPrefs.getLong(Main.SCREEN_DELAY, 0));
		gifPanel.setBorderDepth(savedPrefs.getInt(Main.DEFAULT_BORDER_DEPTH, 0));
		gifPanel.setCaptionFont(new Font(savedPrefs.get(Main.DEFAULT_CAPTION_FONT_NAME, null)
							 		   , savedPrefs.getInt(Main.DEFAULT_CAPTION_FONT_STYLE, 0)
							 		   , savedPrefs.getInt(Main.DEFAULT_CAPTION_FONT_SIZE, 0)));
		gifPanel.setCaptionColor(new Color(savedPrefs.getInt(Main.DEFAULT_CAPTION_COLOR, 0)));
		gifPanel.setBorderColor(new Color(savedPrefs.getInt(Main.DEFAULT_BORDER_COLOR, 0)));
		gifPanel.setSizeChangeListener(new SizeChangeListener(){

			@Override
			public void SizeChanged(int newFileSize, Dimension finalSize)
			{
				notSaved = true;
				// TODO Auto-generated method stub
				Long targetSize = savedPrefs.getLong(Main.TARGET_FILESIZE, Long.MIN_VALUE);
				System.err.println(targetSize + " VS " + newFileSize);
				Color c = Color.GREEN.darker().darker();
				if(newFileSize > targetSize){
					c = Color.RED;
				}
				EditFrame.this.fileSizeLabel.setForeground(c);
				EditFrame.this.fileSizeLabel.setText(humanReadableByteCount(newFileSize, false));
				EditFrame.this.scalingAndAnimationPanel.setScaleFactor(gifPanel.getScalingFactor());
				EditFrame.this.scalingAndAnimationPanel.setDimensionText(finalSize);
			}
			
		});
		gifContainer.add(gifPanel,null);
		
		
		final JPanel controlPanel = new JPanel();
		splitPane.setRightComponent(controlPanel);
		//contentPane.add(controlPanel, BorderLayout.EAST);
		GridBagLayout gbl_controlPanel = new GridBagLayout();
		gbl_controlPanel.columnWidths = new int[] {0, 0};
		gbl_controlPanel.rowHeights = new int[] {0, 0, 0};
		gbl_controlPanel.columnWeights = new double[]{0.0, 1.0};
		gbl_controlPanel.rowWeights = new double[]{1.0, 0.0, 0.0};
		controlPanel.setLayout(gbl_controlPanel);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		GridBagConstraints gbc_tabbedPane = new GridBagConstraints();
		gbc_tabbedPane.gridwidth = 2;
		gbc_tabbedPane.insets = new Insets(0, 0, 5, 0);
		gbc_tabbedPane.fill = GridBagConstraints.BOTH;
		gbc_tabbedPane.gridx = 0;
		gbc_tabbedPane.gridy = 0;
		controlPanel.add(tabbedPane, gbc_tabbedPane);
		
		CaptionPanel captionPanel = new CaptionPanel(this.gifPanel, this.savedPrefs);
		tabbedPane.addTab("Caption", null, captionPanel, null);
		
		OverlayPanel overlayPanel = new OverlayPanel(this.gifPanel);
		tabbedPane.addTab("Overlay", null, overlayPanel, null);
		
		ColorPanel colorPanel = new ColorPanel(this.gifPanel);
		tabbedPane.addTab("Color", null, colorPanel, null);
		
		scalingAndAnimationPanel = new ScalingPanel(gifPanel, savedPrefs);
		tabbedPane.addTab("Scaling", null, scalingAndAnimationPanel, null);
		
		AnimationPanel animationPanel = new AnimationPanel(gifPanel); 
		tabbedPane.addTab("Animation", null, animationPanel, null);
		
		
		JLabel lblFileSize = new JLabel("File Size:");
		lblFileSize.setToolTipText(TOOLTIP_FILE_SIZE);
		GridBagConstraints gbc_lblFileSize = new GridBagConstraints();
		gbc_lblFileSize.anchor = GridBagConstraints.EAST;
		gbc_lblFileSize.insets = new Insets(0, 0, 5, 5);
		gbc_lblFileSize.gridx = 0;
		gbc_lblFileSize.gridy = 1;
		controlPanel.add(lblFileSize, gbc_lblFileSize);
		
		fileSizeLabel = new JLabel("N/A");
		fileSizeLabel.setToolTipText(TOOLTIP_FILE_SIZE);
		GridBagConstraints gbc_fileSizeLabel = new GridBagConstraints();
		gbc_fileSizeLabel.anchor = GridBagConstraints.EAST;
		gbc_fileSizeLabel.insets = new Insets(0, 0, 5, 0);
		gbc_fileSizeLabel.gridx = 1;
		gbc_fileSizeLabel.gridy = 1;
		controlPanel.add(fileSizeLabel, gbc_fileSizeLabel);
		
		JButton saveButton = new JButton("Save");
		saveButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser saveDialog = new JFileChooser();
				
				saveDialog.setFileFilter(new FileNameExtensionFilter("Animated GIF File","gif"));
				if(saveDialog.showSaveDialog(EditFrame.this)== JFileChooser.APPROVE_OPTION){
					try{
						String location = saveDialog.getSelectedFile().getAbsolutePath();
						if(!location.toLowerCase().endsWith(".gif")){
							location = location + ".gif";
						}
						boolean cancel = false;
						if(new File(location).exists()){
							cancel = JOptionPane.YES_OPTION != JOptionPane.showConfirmDialog(EditFrame.this, "File already exists, do you want to overwrite?", "Overwrite?", JOptionPane.YES_NO_CANCEL_OPTION);
						}
						if(!cancel){
							getGifPanel().saveImage(location);
							notSaved = false;
						}
					} catch (Exception ex){
						JOptionPane.showMessageDialog(EditFrame.this, ex.getMessage());
					}
				}
			}
		});
		GridBagConstraints gbc_saveButton = new GridBagConstraints();
		gbc_saveButton.insets = new Insets(0, 0, 5, 0);
		gbc_saveButton.gridwidth = 2;
		gbc_saveButton.fill = GridBagConstraints.HORIZONTAL;
		gbc_saveButton.gridx = 0;
		gbc_saveButton.gridy = 2;
		controlPanel.add(saveButton, gbc_saveButton);
		
		
		this.addWindowListener(new WindowListener(){

			@Override
			public void windowActivated(WindowEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void windowClosed(WindowEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void windowClosing(WindowEvent arg0) {
				boolean close = !notSaved;
				if(!close){
					int returnVal = JOptionPane.showConfirmDialog(EditFrame.this, "GIF has not be saved, Are you sure you want to close?", "Are you sure you want to close?", JOptionPane.YES_NO_CANCEL_OPTION);
					close = returnVal == JOptionPane.YES_OPTION;
				}
				
				if(close)
					System.exit(0);
			}

			@Override
			public void windowDeactivated(WindowEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void windowDeiconified(WindowEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void windowIconified(WindowEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void windowOpened(WindowEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
		});
		
		this.addComponentListener(new ComponentListener(){

			@Override
			public void componentHidden(ComponentEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void componentMoved(ComponentEvent e) {
				EditFrame.this.savedPrefs.putInt(Main.EDIT_X, EditFrame.this.getX());
				EditFrame.this.savedPrefs.putInt(Main.EDIT_Y, EditFrame.this.getY());
				try {
					EditFrame.this.savedPrefs.flush();
				} catch (BackingStoreException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}

			@Override
			public void componentResized(ComponentEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void componentShown(ComponentEvent e) {
				// TODO Auto-generated method stub
				
			}
			
		});
		
		
		// This code is to allow for proper firing of JSpinner events
		// http://stackoverflow.com/a/7587253/1111886
		for (Component c : this.getRootPane().getComponents()) {
			if(c instanceof JSpinner){
				JComponent comp = ((JSpinner) c).getEditor();
			    JFormattedTextField field = (JFormattedTextField) comp.getComponent(0);
			    DefaultFormatter formatter = (DefaultFormatter) field.getFormatter();
			    formatter.setCommitsOnValidEdit(true);
			}
		}
		
		
		this.pack();
		this.setLocation(savedPrefs.getInt(Main.EDIT_X, 0), savedPrefs.getInt(Main.EDIT_Y, 0));
		if(!this.isInScreenBounds()){
			GraphicsEnvironment graphicsEnvironment = GraphicsEnvironment.getLocalGraphicsEnvironment();
		    GraphicsDevice[] graphicsDevices = graphicsEnvironment.getScreenDevices();
		    Rectangle b = graphicsDevices[0].getDefaultConfiguration().getBounds();
		    
		    this.setLocation(b.x + ((b.width-this.getWidth())/2), b.y + ((b.height-this.getHeight())/2));
		}
	}
	
    public boolean isInScreenBounds() 
    {
		// Check if the location is in the bounds of one of the graphics devices.
		GraphicsEnvironment graphicsEnvironment = GraphicsEnvironment.getLocalGraphicsEnvironment();
		GraphicsDevice[] graphicsDevices = graphicsEnvironment.getScreenDevices();
		Rectangle graphicsConfigurationBounds = new Rectangle();
		
		boolean topLeftInBounds = false,bottomRightInBounds = false;
		boolean isOnScreen = false;
		// Iterate over the graphics devices.
		for (int j = 0; !isOnScreen && j < graphicsDevices.length; j++) {	  
			// Get the bounds of the device.
			GraphicsDevice graphicsDevice = graphicsDevices[j];
			graphicsConfigurationBounds.setRect(graphicsDevice.getDefaultConfiguration().getBounds());
			  
			// Is the location in this bounds?
			//graphicsConfigurationBounds.setRect(graphicsDevice.getDefaultConfiguration().getBounds());
			//graphicsConfigurationBounds.setRect(graphicsConfigurationBounds.x, graphicsConfigurationBounds.y,
			//    graphicsConfigurationBounds.width, graphicsConfigurationBounds.height);
			
		    topLeftInBounds = graphicsConfigurationBounds.contains(this.getX(),this.getY());
		
		    bottomRightInBounds= graphicsConfigurationBounds.contains(this.getX() + this.getWidth(), this.getY() + this.getHeight());
		    
		    isOnScreen = bottomRightInBounds && topLeftInBounds;
		}
    
	    // We could not find a device that contains the given point.
	    return isOnScreen;
    }
	
	public static String humanReadableByteCount(long bytes, boolean si) {
	    int unit = si ? 1000 : 1024;
	    if (bytes < unit) return bytes + " B";
	    int exp = (int) (Math.log(bytes) / Math.log(unit));
	    String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp-1) + (si ? "" : "i");
	    return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
	}

	public GifPanel getGifPanel() {
		return gifPanel;
	}
	public JPanel getContentPane() {
		return contentPane;
	}
	public JLabel getFileSizeLabel() {
		return fileSizeLabel;
	}
	protected JLabel getLblFinalDimensions() {
		return lblFinalDimensions;
	}
}
