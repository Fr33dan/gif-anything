package com.clayton.gifanything.gui.edit;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.clayton.gifanything.Main;
import com.clayton.gifanything.gui.FrameInfo;
import com.clayton.gifanything.gui.FramePanel;
import com.clayton.gifanything.gui.GifPanel;
import com.clayton.gifanything.gui.ModifiedFlowLayout;
import com.clayton.gifanything.gui.WrapLayout;

import javax.swing.JCheckBox;

import java.beans.Beans;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;

import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import java.awt.FlowLayout;

public class AnimationPanel extends JPanel {

	private GifPanel gifPanel;
	private JSpinner startFrameSpinner;
	private JSpinner endFrameSpinner;
	private JLabel lblStartFrame;
	private JLabel lblEndFrame;
	private JScrollPane framesScrollPane;
	private JPanel framesPanel;

	/**
	 * Create the panel.
	 */
	public AnimationPanel(GifPanel panel) {
		gifPanel = panel;
		GridBagLayout gbl_scalingAndAnimationPanel = new GridBagLayout();
		gbl_scalingAndAnimationPanel.columnWidths = new int[] {0, 0};
		gbl_scalingAndAnimationPanel.rowHeights = new int[]{0, 0, 0, 0, 0, };
		gbl_scalingAndAnimationPanel.columnWeights = new double[]{1.0, 0.0};
		gbl_scalingAndAnimationPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		this.setLayout(gbl_scalingAndAnimationPanel);
		lblStartFrame = new JLabel("Start Frame:");
		GridBagConstraints gbc_lblStartFrame = new GridBagConstraints();
		gbc_lblStartFrame.anchor = GridBagConstraints.EAST;
		gbc_lblStartFrame.insets = new Insets(0, 0, 5, 5);
		gbc_lblStartFrame.gridx = 0;
		gbc_lblStartFrame.gridy = 0;
		this.add(lblStartFrame, gbc_lblStartFrame);
		lblStartFrame.setToolTipText(EditFrame.TOOLTIP_FIRST_FRAME);
		
		startFrameSpinner = new JSpinner();
		GridBagConstraints gbc_startFrameSpinner = new GridBagConstraints();
		gbc_startFrameSpinner.fill = GridBagConstraints.HORIZONTAL;
		gbc_startFrameSpinner.insets = new Insets(0, 0, 5, 0);
		gbc_startFrameSpinner.gridx = 1;
		gbc_startFrameSpinner.gridy = 0;
		this.add(startFrameSpinner, gbc_startFrameSpinner);
		startFrameSpinner.setToolTipText(EditFrame.TOOLTIP_FIRST_FRAME);
		
		lblEndFrame = new JLabel("End Frame:");
		GridBagConstraints gbc_lblEndFrame = new GridBagConstraints();
		gbc_lblEndFrame.anchor = GridBagConstraints.EAST;
		gbc_lblEndFrame.insets = new Insets(0, 0, 5, 5);
		gbc_lblEndFrame.gridx = 0;
		gbc_lblEndFrame.gridy = 1;
		this.add(lblEndFrame, gbc_lblEndFrame);
		lblEndFrame.setToolTipText(EditFrame.TOOLTIP_FINAL_FRAME);
		
		endFrameSpinner = new JSpinner();
		GridBagConstraints gbc_endFrameSpinner = new GridBagConstraints();
		gbc_endFrameSpinner.insets = new Insets(0, 0, 5, 0);
		gbc_endFrameSpinner.fill = GridBagConstraints.HORIZONTAL;
		gbc_endFrameSpinner.gridx = 1;
		gbc_endFrameSpinner.gridy = 1;
		this.add(endFrameSpinner, gbc_endFrameSpinner);
		endFrameSpinner.setToolTipText(EditFrame.TOOLTIP_FINAL_FRAME);
		endFrameSpinner.setModel(new SpinnerNumberModel(gifPanel.getFrameCount() - 1, 0, gifPanel.getFrameCount() - 1, 1));
		
		final JCheckBox chckbxAdvanced = new JCheckBox("Advanced");
		GridBagConstraints gbc_chckbxAdvanced = new GridBagConstraints();
		gbc_chckbxAdvanced.gridwidth = 2;
		gbc_chckbxAdvanced.insets = new Insets(0, 0, 5, 0);
		gbc_chckbxAdvanced.anchor = GridBagConstraints.EAST;
		gbc_chckbxAdvanced.gridx = 0;
		gbc_chckbxAdvanced.gridy = 2;
		add(chckbxAdvanced, gbc_chckbxAdvanced);
		chckbxAdvanced.setHorizontalTextPosition(SwingConstants.LEFT);
		
		framesScrollPane = new JScrollPane();
		framesScrollPane.setVisible(false);
		framesScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		framesScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		GridBagConstraints gbc_framesScrollPane = new GridBagConstraints();
		gbc_framesScrollPane.gridwidth = 2;
		gbc_framesScrollPane.fill = GridBagConstraints.BOTH;
		gbc_framesScrollPane.gridx = 0;
		gbc_framesScrollPane.gridy = 3;
		add(framesScrollPane, gbc_framesScrollPane);
		
		framesPanel = new JPanel();
		ModifiedFlowLayout framesPanelLayout = new ModifiedFlowLayout(FlowLayout.LEFT);
		framesPanel.setLayout(framesPanelLayout);
		
		if(!Beans.isDesignTime()){
			FramePanel newPanel = null;
			for(int j = 0;j < this.gifPanel.getFrameCount();j++){
				newPanel = new FramePanel(this.gifPanel,j);
				this.framesPanel.add(newPanel);
			}
			//framesPanel.setMaximumSize(new Dimension((newPanel.getPreferredSize().width +framesPanelLayout.getHgap()) * 2,Integer.MAX_VALUE));
			//framesPanel.setPreferredSize(this.getMaximumSize());
			int width = (newPanel.getPreferredSize().width +framesPanelLayout.getHgap());
			int height = (newPanel.getPreferredSize().height +framesPanelLayout.getVgap());
			//framesPanel.setSize(panelSize);
			Dimension newSize = new Dimension(width * 2,height * (gifPanel.getFrameCount()/2));
			//framesPanel.setPreferredSize(newSize);
			//framesPanel.setMaximumSize(newSize);
			//framesPanel.setMinimumSize(new Dimension(width,gifPanel.getFrameCount()));
			framesScrollPane.setViewportView(framesPanel);
			
			int prefferedWidth = width * 2 + framesPanelLayout.getHgap() + framesScrollPane.getVerticalScrollBar().getPreferredSize().width;
			int prefferedHeight = this.getPreferredSize().height;
			framesScrollPane.setPreferredSize(new Dimension(prefferedWidth,1));
			this.setPreferredSize(new Dimension(prefferedWidth,prefferedHeight));
		}
		endFrameSpinner.addChangeListener(new ChangeListener() {

		    @SuppressWarnings("unchecked")
			@Override
		    public void stateChanged(ChangeEvent e) {
		    	for(int j = (int)startFrameSpinner.getValue();j < gifPanel.getFrameCount();j++)
		    	{
		    		FrameInfo data = gifPanel.getFrameInfo(j);
		    		data.setEnabled(j <= (int)endFrameSpinner.getValue());
		    		//gifPanel.setFrameInfo(data);
		    	}
		    	
		    	SpinnerNumberModel model = (SpinnerNumberModel) startFrameSpinner.getModel();
		    	model.setMaximum((Comparable<Integer>) endFrameSpinner.getValue());
		    }
		});
		
		startFrameSpinner.addChangeListener(new ChangeListener() {

		    @SuppressWarnings("unchecked")
			@Override
		    public void stateChanged(ChangeEvent e) {
		    	for(int j = 0;j < (int)endFrameSpinner.getValue();j++)
		    	{
		    		FrameInfo data = gifPanel.getFrameInfo(j);
		    		data.setEnabled(j >= (int)startFrameSpinner.getValue());
		    		//gifPanel.setFrameInfo(data);
		    	}

		    	SpinnerNumberModel model = (SpinnerNumberModel) endFrameSpinner.getModel();
		    	model.setMinimum((Comparable<Integer>) startFrameSpinner.getValue());
		    }
		});

		chckbxAdvanced.addPropertyChangeListener(new PropertyChangeListener() {
			public void propertyChange(PropertyChangeEvent arg0) {
				if(chckbxAdvanced.isSelected() != framesScrollPane.isVisible()){
					boolean advancedEnabled = chckbxAdvanced.isSelected();
					lblStartFrame.setEnabled(!advancedEnabled);
					startFrameSpinner.setEnabled(!advancedEnabled);
					lblEndFrame.setEnabled(!advancedEnabled);
					endFrameSpinner.setEnabled(!advancedEnabled);
					framesScrollPane.setVisible(advancedEnabled);
					if(Main.log)
						System.err.println(System.currentTimeMillis() + " : setVisible called");
					//AnimationPanel.this.invalidate();
					framesScrollPane.invalidate();
					if(!advancedEnabled){
						for(int j = 0; j < gifPanel.getFrameCount();j++){
							gifPanel.getFrameInfo(j).resetDelay();
						}
					}
				}
			}
		});
	}
}
