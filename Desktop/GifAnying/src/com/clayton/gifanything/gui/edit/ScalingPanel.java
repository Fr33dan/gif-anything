package com.clayton.gifanything.gui.edit;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.prefs.Preferences;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;

import com.clayton.gifanything.Main;
import com.clayton.gifanything.gui.GifPanel;

import javax.swing.SwingConstants;

public class ScalingPanel extends JPanel {

	private GifPanel gifPanel;
	private JTextField scalingField;
	private JSlider scalingSlider;
	private JLabel lblFinalDimensions;
	private JButton scaleToSizeButton;

	private DocumentListener editListener;
	/**
	 * Create the panel.
	 */
	public ScalingPanel(GifPanel panel,final Preferences savedPrefs) {
		gifPanel = panel;
		GridBagLayout gbl_scalingAndAnimationPanel = new GridBagLayout();
		gbl_scalingAndAnimationPanel.columnWidths = new int[] {0, 0, 0};
		gbl_scalingAndAnimationPanel.rowHeights = new int[]{0, 0, 0, 0, 0, 0};
		gbl_scalingAndAnimationPanel.columnWeights = new double[]{0.0, 1.0, 0.0};
		gbl_scalingAndAnimationPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		this.setLayout(gbl_scalingAndAnimationPanel);
		
		JLabel lblScaling = new JLabel("Scaling:");
		lblScaling.setToolTipText(EditFrame.TOOLTIP_SCALING);
		GridBagConstraints gbc_lblScaling = new GridBagConstraints();
		gbc_lblScaling.anchor = GridBagConstraints.EAST;
		gbc_lblScaling.insets = new Insets(0, 0, 5, 5);
		gbc_lblScaling.gridx = 0;
		gbc_lblScaling.gridy = 0;
		this.add(lblScaling, gbc_lblScaling);
		
		scalingField = new JTextField();
		scalingField.setText("100%");
		scalingField.setToolTipText(EditFrame.TOOLTIP_SCALING);
		this.editListener = new DocumentListener(){

			public void update(DocumentEvent arg0){
				try
				{
					String newValue = arg0.getDocument().getText(0, arg0.getDocument().getLength());
					//if(!newValue.equals("") && gifPanel != null && !(scalingSlider!=null && scalingSlider.getValueIsAdjusting())){
					if(!newValue.equals("") && gifPanel != null){
						if(newValue.endsWith("%"))
							newValue = newValue.substring(0, newValue.length() - 1);
						
						double newScalingValue = Double.parseDouble(newValue)/100;
						if(scalingSlider!=null && !scalingSlider.getValueIsAdjusting()){
							scalingSlider.setValue((int)(newScalingValue * 100));
						}
						gifPanel.setScalingFactor(newScalingValue);
						ScalingPanel.this.setDimensionText(gifPanel.getFinalSize());
					}
				}
				catch (BadLocationException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			@Override
			public void changedUpdate(DocumentEvent arg0)
			{
				update(arg0);
			}

			@Override
			public void insertUpdate(DocumentEvent arg0)
			{	
				update(arg0);
			}

			@Override
			public void removeUpdate(DocumentEvent arg0)
			{	
				update(arg0);
			}
			
		};
		scalingField.getDocument().addDocumentListener(editListener);
		GridBagConstraints gbc_scalingField = new GridBagConstraints();
		gbc_scalingField.anchor = GridBagConstraints.EAST;
		gbc_scalingField.insets = new Insets(0, 0, 5, 0);
		gbc_scalingField.gridx = 2;
		gbc_scalingField.gridy = 0;
		this.add(scalingField, gbc_scalingField);
		scalingField.setColumns(4);
		
		scalingSlider = new JSlider();
		scalingSlider.setToolTipText(EditFrame.TOOLTIP_SCALING);
		scalingSlider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				if(scalingSlider.getValueIsAdjusting()){
					scalingField.setText(scalingSlider.getValue() + "%");
				}
			}
		});
		GridBagConstraints gbc_scalingSlider = new GridBagConstraints();
		gbc_scalingSlider.fill = GridBagConstraints.HORIZONTAL;
		gbc_scalingSlider.insets = new Insets(0, 0, 5, 5);
		gbc_scalingSlider.gridx = 1;
		gbc_scalingSlider.gridy = 0;
		this.add(scalingSlider, gbc_scalingSlider);
		scalingSlider.setValue(100);
		
		JButton btnRemoveCropping = new JButton("Remove Cropping");
		btnRemoveCropping.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ScalingPanel.this.gifPanel.resetCropping();
			}
		});
		
		lblFinalDimensions = new JLabel("Final Dimensions:");
		GridBagConstraints gbc_lblFinalDimensions = new GridBagConstraints();
		gbc_lblFinalDimensions.anchor = GridBagConstraints.EAST;
		gbc_lblFinalDimensions.gridwidth = 3;
		gbc_lblFinalDimensions.insets = new Insets(0, 0, 5, 0);
		gbc_lblFinalDimensions.gridx = 0;
		gbc_lblFinalDimensions.gridy = 1;
		this.add(lblFinalDimensions, gbc_lblFinalDimensions);
		GridBagConstraints gbc_btnRemoveCropping = new GridBagConstraints();
		gbc_btnRemoveCropping.anchor = GridBagConstraints.EAST;
		gbc_btnRemoveCropping.gridwidth = 2;
		gbc_btnRemoveCropping.insets = new Insets(0, 0, 5, 0);
		gbc_btnRemoveCropping.gridx = 1;
		gbc_btnRemoveCropping.gridy = 2;
		this.add(btnRemoveCropping, gbc_btnRemoveCropping);
		
		scaleToSizeButton = new JButton("Scale to target size");
		scaleToSizeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ScalingPanel.this.gifPanel.seekTargetSize(savedPrefs.getLong(Main.TARGET_FILESIZE, 1024*1024));
			}
		});
		GridBagConstraints gbc_scaleToSizeButton = new GridBagConstraints();
		gbc_scaleToSizeButton.anchor = GridBagConstraints.EAST;
		gbc_scaleToSizeButton.gridwidth = 2;
		gbc_scaleToSizeButton.insets = new Insets(0, 0, 5, 0);
		gbc_scaleToSizeButton.gridx = 1;
		gbc_scaleToSizeButton.gridy = 3;
		add(scaleToSizeButton, gbc_scaleToSizeButton);
	}
	
	public void setScaleFactor(double factor){
		this.scalingField.getDocument().removeDocumentListener(editListener);
		this.scalingSlider.setValue((int) Math.round(factor * 100));
		this.scalingField.setText((factor * 100) + "%");
		this.scalingField.getDocument().addDocumentListener(editListener);
	}

	public void setDimensionText(Dimension size) {
		this.lblFinalDimensions.setText("Final dimensions: " + size.width + " x " + size.height + "px");
		
	}

}
