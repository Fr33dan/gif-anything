package com.clayton.gifanything;

import java.awt.AWTException;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.prefs.Preferences;

import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import com.clayton.gifanything.gui.LoadingFrame;
import com.clayton.gifanything.gui.edit.EditFrame;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.rmi.ObjectSpace;

public class MonitorClient implements IMonitorClient {
	public static final int OBJECT_ID = 12;
	volatile MonitorScreen monitor;
	Preferences savedPreferences;
	ObjectSpace serverSpace;
	
	public MonitorClient() throws IOException, AWTException, ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException{

		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		savedPreferences = Preferences.userNodeForPackage(Main.class);
		monitor = new MonitorScreen();
		monitor.setScreenDelay(savedPreferences.getLong(Main.SCREEN_DELAY, 67));
		monitor.setScaledWidth(savedPreferences.getInt(Main.SCALED_WIDTH, -1));
		monitor.setScaledHeight(savedPreferences.getInt(Main.SCALED_HEIGHT, -1));
		monitor.setScaledMode(savedPreferences.getBoolean(Main.SCALED_MODE, true));
		

		Client serverClient = new Client();
		serverClient.start();
		serverClient.connect(10000, "127.0.0.1", Main.TCP_PORT, Main.UDP_PORT);
		
		Kryo kryo = serverClient.getKryo();
		ObjectSpace.registerClasses(kryo);
		
		
		kryo.register(IMonitorClient.class);
		serverSpace = new ObjectSpace();
		serverSpace.register(OBJECT_ID, this);
		serverSpace.addConnection(serverClient);
		
		serverClient.sendTCP("RecordWaiting");
		
		while(true){
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void startTrigger(){
		new Thread(){
			public void run(){
				MonitorClient.this.monitor.startTrigger();
			}
		}.start();
	}
	
	public void stopTrigger(){
		new Thread(){
			public void run(){;
			BufferedImage[] toGif = null;
				try{
					toGif = monitor.endTrigger();
				} catch(Exception e){
					JOptionPane.showMessageDialog(null, "Unable to get gif frames from capture.", "Error", JOptionPane.ERROR_MESSAGE);
					System.exit(0);
				}
				try{
					serverSpace.remove(OBJECT_ID);
				} catch(Exception e){
					JOptionPane.showMessageDialog(null, "Unable to communicate with controling instance.", "Error", JOptionPane.ERROR_MESSAGE);
					
				}
				if(toGif.length != 0){
					try{
						LoadingFrame newFrame = new LoadingFrame();
						newFrame.setLocation(savedPreferences.getInt(Main.EDIT_X, 0), savedPreferences.getInt(Main.EDIT_Y, 0));
						newFrame.setVisible(true);
	
						
						EditFrame editor = new EditFrame(toGif, savedPreferences);
						editor.setVisible(true);
						
						newFrame.dispose();
					} catch(Exception e){
						JOptionPane.showMessageDialog(null, "An error occured opening editing frame.\n" + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
						System.exit(0);
					}
				} else{
					JOptionPane.showMessageDialog(null, "No frames captured. Remember to hold down the capture key.", "No frames", JOptionPane.ERROR_MESSAGE);
					System.exit(0);
				}
			}
		}.start();
	}
	

}
