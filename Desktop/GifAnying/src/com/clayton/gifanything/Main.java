package com.clayton.gifanything;

import java.awt.AWTException;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import java.util.prefs.Preferences;

import javax.imageio.ImageIO;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import org.jnativehook.GlobalScreen;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;

import com.clayton.gifanything.gui.HelpFrame;
import com.clayton.gifanything.gui.LoadingFrame;
import com.clayton.gifanything.gui.PreferencesFrame;
import com.clayton.gifanything.gui.edit.EditFrame;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;
import com.esotericsoftware.kryonet.rmi.ObjectSpace;
import com.esotericsoftware.kryonet.rmi.RemoteObject;

public class Main implements ActionListener,NativeKeyListener {
	public static final Image Window_Icon = Toolkit.getDefaultToolkit().getImage(PreferencesFrame.class.getResource("/images/windowicon.png"));
	public static final int UDP_PORT = 45666;
	public static final int TCP_PORT = 45667;
	public static final String CURRENT_VERSION_ID = "1.3.3";
	public static final String EDIT_X = "EDIT_X";
	public static final String EDIT_Y = "EDIT_Y";
	public static final String UPDATE_LOCATION = "http://gifanything.comyr.com/";
	public static final String SCALED_HEIGHT = "SCALED_HEIGHT";
	public static final String SCALED_WIDTH = "SCALED_WIDTH";
	public static final String SCALED_MODE = "SCALED_MODE";
	public static final String SCREEN_DELAY = "SCREEN_DELAY";
	public static final String TARGET_FILESIZE = "TARGET_FILESIZE";
	public static final String DEFAULT_CAPTION_COLOR = "DEFAULT_CAPTION_COLOR";
	public static final String DEFAULT_CAPTION_FONT_NAME = "DEFAULT_CAPTION_FONT_NAME";
	public static final String DEFAULT_CAPTION_FONT_SIZE = "DEFAULT_CAPTION_FONT_SIZE";
	public static final String DEFAULT_CAPTION_FONT_STYLE = "DEFAULT_CAPTION_FONT_STYLE";
	public static final String DEFAULT_BORDER_COLOR = "DEFAULT_BORDER_COLOR";
	public static final String DEFAULT_BORDER_DEPTH = "DEFAULT_BORDER_DEPTH";
	public static final String CAPTURE_KEY = "SCREEN_CAPTURE_KEY_CODE";
	public static final String CAPTURE_MODIFIER = "SCREEN_CAPTURE_MODIFIER_CODE";
	public static final String CHECK_FOR_UPDATES = "CHECK_FOR_UPDATES";
	public static final String UPDATE_CHECK_LENGTH = "UPDATE_CHECK_LENGTH";
	private MenuItem exitItem;
	private MenuItem prefrencesItem;
	private MenuItem helpItem;
	private Preferences savedPreferences;
	private static UpdateManagement updateManager;
	
	private static boolean debug;
	public static boolean log;
	
	private PreferencesFrame prefFrame;
	private HelpFrame helpFrame;
	
	private Server controllerServer;
	private volatile IMonitorClient remoteClient;
	private int captureCode;
	private int captureModifier;
	private TrayIcon myTray;
	private boolean remoteStarted;
	
	public static UpdateManagement getUpdateManager(){
		return updateManager;
	}
	
	
	public static void main(String[] args){
		boolean record = false;
		for(int j = 0; j < args.length;j++){
			if(args[j].equalsIgnoreCase("-ResetStartup")){
				StartupManagement.resetStartupItem();
			} else if(args[j].equalsIgnoreCase("-RemoveOldVersion")){
				final String oldVersion = args[++j];
				
				Thread deleteThread = new Thread(new Runnable(){
					@Override
					public void run() {
						File oldVersionFile = new File(oldVersion);
						while(oldVersionFile.exists()){
							//JOptionPane.showMessageDialog(null, "Attempting to delete");
							oldVersionFile.delete();
						}
					}
				});
				deleteThread.start();
			} else if(args[j].equalsIgnoreCase("-Record")){
				record = true;
			} else if(args[j].equalsIgnoreCase("-Debug")){
				debug = true;
			}
		}
		if(record){
			try {
				new MonitorClient();
			} catch (IOException | AWTException | ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
				JOptionPane.showMessageDialog(null, "Error recording");
			}
		} else{
			new Main();
		}
	}
	public static void exit(){
		//JOptionPane.showMessageDialog(null, "Exit");
		//try{
		//	GlobalScreen.unregisterNativeHook();
		//}catch(Exception e){}
		System.exit(0);
	}
	public void reloadPreferences(){
		this.captureCode = savedPreferences.getInt(CAPTURE_KEY, NativeKeyEvent.VC_F12);
		this.captureModifier = savedPreferences.getInt(CAPTURE_MODIFIER, 0);
		
		UpdateManagement manager = Main.getUpdateManager();
		if(manager !=null){
			if(savedPreferences.getBoolean(CHECK_FOR_UPDATES, true)){
				manager.startUpdateChecking();
			} else{
				manager.stopUpdateChecking();
			}
		}
	}
	
	public Main(){
		URL iconURL = Main.class.getResource("/images/trayicon.png");
		try {
			this.controllerServer = new Server();
			this.controllerServer.start();
			this.controllerServer.bind(TCP_PORT, UDP_PORT);
			Kryo kryo = this.controllerServer.getKryo();
			ObjectSpace.registerClasses(kryo);
			kryo.register(IMonitorClient.class);
			
			this.controllerServer.addListener(new Listener(){
				public void received(final Connection c,Object obj){
					if(obj instanceof String && "RecordWaiting".equalsIgnoreCase((String) obj)){
						new Thread(){
							public void run(){
								try{
									Main.this.remoteClient = ObjectSpace.getRemoteObject(c, MonitorClient.OBJECT_ID, IMonitorClient.class);
									
									// Eliminates the error, but my later calls
									// do no go through.
									//((RemoteObject)remoteClient).setNonBlocking(true); 
									((RemoteObject)remoteClient).setTransmitReturnValue(false);
									remoteClient.startTrigger();
									if(!remoteStarted){
										remoteClient.stopTrigger();
										remoteClient = null;
									}
								}catch(Exception e)
								{
									e.printStackTrace();
								}
							}
						}.start();
					}
				}
			});
			
			
			
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			
			savedPreferences = Preferences.userNodeForPackage(Main.class);
			
			if(savedPreferences.getInt(SCALED_WIDTH, -1) == -1){
				// Initialize all preferences to default values
				savedPreferences.putInt(SCALED_HEIGHT, screenSize.height/2);
				savedPreferences.putInt(SCALED_WIDTH, screenSize.width/2);
				savedPreferences.putBoolean(SCALED_MODE, true);
				savedPreferences.putBoolean(CHECK_FOR_UPDATES, true);
				savedPreferences.putLong(UPDATE_CHECK_LENGTH, 10800000);
				savedPreferences.putLong(SCREEN_DELAY, 67);
				savedPreferences.putLong(TARGET_FILESIZE, 1024*1024);
				
				savedPreferences.put(DEFAULT_CAPTION_FONT_NAME, "Arial");
				savedPreferences.putInt(DEFAULT_CAPTION_FONT_SIZE, 22);
				savedPreferences.putInt(DEFAULT_CAPTION_FONT_STYLE, Font.BOLD);
				savedPreferences.putInt(DEFAULT_CAPTION_COLOR, Color.WHITE.getRGB());
				savedPreferences.putInt(DEFAULT_BORDER_COLOR, Color.BLACK.getRGB());
				savedPreferences.putInt(DEFAULT_BORDER_DEPTH, 3);
				savedPreferences.flush();
			}
			myTray = new TrayIcon(ImageIO.read(iconURL));
			
			myTray.setImageAutoSize(true);
			myTray.setToolTip("GifAnything");
			PopupMenu systemMenu = new PopupMenu();

			
			prefrencesItem = new MenuItem("Preferences");
			prefrencesItem.addActionListener(this);
			systemMenu.add(prefrencesItem);
			
			helpItem = new MenuItem("Help");
			helpItem.addActionListener(this);
			systemMenu.add(helpItem);
			
			exitItem = new MenuItem("Exit");
			exitItem.addActionListener(this);
			systemMenu.add(exitItem);
			
			myTray.setPopupMenu(systemMenu);
			myTray.addMouseListener(new MouseAdapter(){
				public void mousePressed(MouseEvent e ) {
					if(e.getButton() == MouseEvent.BUTTON1){
						openPreferences();
					}
			     }
			});
			
			SystemTray.getSystemTray().add(myTray);


			// Clear previous logging configurations.
			LogManager.getLogManager().reset();

			// Get the logger for "org.jnativehook" and set the level to off.
			Logger logger = Logger.getLogger(GlobalScreen.class.getPackage().getName());
			logger.setLevel(Level.OFF);
			
			GlobalScreen.registerNativeHook();
			
			GlobalScreen.getInstance().addNativeKeyListener(this);
			
			Main.updateManager = new UpdateManagement(myTray, savedPreferences);
			
			this.reloadPreferences();
		} catch (AWTException e) {
			JOptionPane.showMessageDialog(null, "GifAnything could not access the system tray and will now close.", "Unable to access tray", JOptionPane.ERROR_MESSAGE);
			System.exit(-1);
		} catch (Exception e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, e.getMessage(), "Unable to start software", JOptionPane.ERROR_MESSAGE);
			
			System.exit(-1);
		}

		prefFrame = new PreferencesFrame(Main.this.savedPreferences);
		prefFrame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent arg0) {
				Main.this.reloadPreferences();
			}
		});
		
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		if(arg0.getSource() == this.exitItem){
			Main.exit();
		} else if(arg0.getSource()== this.helpItem){
			if(helpFrame == null)
				helpFrame = new HelpFrame(this.savedPreferences);
			helpFrame.setVisible(true);
		} else if(arg0.getSource() == this.prefrencesItem){
			this.openPreferences();
		} 
	}
	
	@Override
	public void nativeKeyPressed(NativeKeyEvent arg0)
	{
		if(arg0.getKeyCode() == this.captureCode && arg0.getModifiers() == this.captureModifier && !this.remoteStarted && this.remoteClient == null){
			try {
				if(Main.log)
					System.err.println("Trigger Started");
				this.remoteStarted = true;
				if(!debug){
					URL oldFileJar = Main.class.getProtectionDomain().getCodeSource().getLocation();
					File oldFile;
					try {
					  oldFile = new File(oldFileJar.toURI());
					} catch(URISyntaxException e2) {
					  oldFile = new File(oldFileJar.getPath());
					}
					Runtime.getRuntime().exec(UpdateManagement.getJavaExec() + " -jar \"" + oldFile.toString() + "\" -Record");
				}
				//this.remoteStarted = true;
			} catch (IOException e) {
				JOptionPane.showMessageDialog(null,"Could not start process", "Unable to access tray", JOptionPane.ERROR_MESSAGE);
				
			}
			
		}
	}

	@Override
	public void nativeKeyReleased(NativeKeyEvent arg0)
	{
		if(arg0.getKeyCode() == this.captureCode && this.remoteStarted){
			if(Main.log)
				System.err.println("Trigger Stopped");
			this.remoteStarted = false;
			if(this.remoteClient != null){
				this.remoteClient.stopTrigger();
				this.remoteClient = null;
			}
		}
	}

	@Override
	public void nativeKeyTyped(NativeKeyEvent arg0)
	{
		
	}
	private void openPreferences() {
		prefFrame.setVisible(true);
	}

}
