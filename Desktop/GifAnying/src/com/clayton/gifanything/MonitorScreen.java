package com.clayton.gifanything;

import java.awt.AWTException;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class MonitorScreen {
	private class MonitorTask extends TimerTask{

		@Override
		public void run()
		{
			MonitorScreen.this.timerUpdate();
		}
		
	};
	private Robot screenGrabber;
	private Rectangle screenSize;
	private volatile List<BufferedImage> triggerModeScreens;
	public volatile boolean paused;
	private long screenDelay = 200;
	
	private int scaledWidth = -1;
	private int scaledHeight = -1;
	
	private boolean scaledMode;
	
	private Timer runningTimer;
	public MonitorScreen() throws AWTException{
		this.screenSize = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
		this.screenGrabber = new Robot();
		
	}
	
	private void timerUpdate() {
		if(!paused){
			BufferedImage newScreen = this.screenGrabber.createScreenCapture(screenSize);
			
			if(this.scaledMode){
				newScreen = MonitorScreen.toBufferedImage(newScreen.getScaledInstance(this.getScaledWidth(), this.getScaledHeight(), BufferedImage.SCALE_FAST));
			}
			
			if(this.triggerModeScreens!=null)
				this.triggerModeScreens.add(newScreen);
			
		}
	}
	
	public void startTrigger(){
		this.triggerModeScreens = new ArrayList<BufferedImage>();
		this.paused = false;

		if(this.runningTimer!=null)
			throw new IllegalStateException("Monitor already started");
		
		if(this.scaledMode){
			if(this.getScaledHeight() == -1)
				throw new IllegalStateException("Scaled Mode enabled without scaling values set");

			if(this.getScaledWidth() == -1)
				throw new IllegalStateException("Scaled Mode enabled without scaling values set");	
		}
		
		this.runningTimer = new Timer();
		
		this.runningTimer.scheduleAtFixedRate(new MonitorTask(), 0, this.getScreenDelay());
	}
	
	public BufferedImage[] endTrigger(){
		// Stops any new images from being added to the list.
		this.paused = true;
		this.runningTimer.cancel();
		this.runningTimer = null;
		
		BufferedImage[] returnVal = this.triggerModeScreens.toArray(new BufferedImage[this.triggerModeScreens.size()]);
		this.triggerModeScreens = null;
		
		return returnVal;
	}
	
	public boolean isRunning(){
		return this.runningTimer!=null;
	}
	
	public long getScreenDelay()
	{
		return screenDelay;
	}


	public void setScreenDelay(long screenDelay)
	{
		if(this.runningTimer!=null){
			throw new IllegalStateException("Cannot set parameters while monitor is running");
		}
		this.screenDelay = screenDelay;
	}


	public boolean isScaledMode()
	{
		return scaledMode;
	}


	public void setScaledMode(boolean scaledMode)
	{
		if(this.runningTimer!=null){
			throw new IllegalStateException("Cannot set parameters while monitor is running");
		}
		this.scaledMode = scaledMode;
	}


	public int getScaledWidth()
	{
		return scaledWidth;
	}


	public void setScaledWidth(int scaledWidth)
	{
		if(this.runningTimer!=null){
			throw new IllegalStateException("Cannot set parameters while monitor is running");
		}
		this.scaledWidth = scaledWidth;
	}


	public int getScaledHeight()
	{
		return scaledHeight;
	}


	public void setScaledHeight(int scaledHeight)
	{
		if(this.runningTimer!=null){
			throw new IllegalStateException("Cannot set parameters while monitor is running");
		}
		this.scaledHeight = scaledHeight;
	}


	/**
	 * Converts a given Image into a BufferedImage
	 *
	 * @param img The Image to be converted
	 * @return The converted BufferedImage
	 */
	public static BufferedImage toBufferedImage(Image img)
	{
	    if (img instanceof BufferedImage)
	    {
	        return (BufferedImage) img;
	    }

	    // Create a buffered image with transparency
	    BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);

	    // Draw the image on to the buffered image
	    Graphics2D bGr = bimage.createGraphics();
	    bGr.drawImage(img, 0, 0, null);
	    bGr.dispose();

	    // Return the buffered image
	    return bimage;
	}
	
}
