package com.clayton.gifanything.gifmaker;

import java.util.EventListener;

public interface GifProgressListener extends EventListener {
	
	public void onGifProgress(GifProgressEvent args);

}
