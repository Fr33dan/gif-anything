package com.clayton.gifanything.gifmaker;

import java.util.EventObject;

public class GifProgressEvent extends EventObject {
	private String progressString;
	private double completionPercentage;

	public GifProgressEvent(Object arg0,String progressInfo,double percentComplete) {
		super(arg0);
		this.setProgressString(progressInfo);
		this.setCompletionPercentage(percentComplete);
	}

	public String getProgressString() {
		return progressString;
	}

	public void setProgressString(String progressString) {
		this.progressString = progressString;
	}

	public double getCompletionPercentage() {
		return completionPercentage;
	}

	public void setCompletionPercentage(double completionPercentage) {
		this.completionPercentage = completionPercentage;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
