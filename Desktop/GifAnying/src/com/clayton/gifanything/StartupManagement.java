package com.clayton.gifanything;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;

public class StartupManagement {
	public static void resetStartupItem(){
		if(StartupManagement.isStartupItemSupported()){
			try{
				if(StartupManagement.isStartupItem()){
					StartupManagement.setStartupItem(false);
					StartupManagement.setStartupItem(true);
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}

	public static void setStartupItem(boolean startupItem) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException, FileNotFoundException{
		String os = System.getProperty("os.name").toLowerCase();
		if(startupItem){
			if(os.contains("windows")){
				WinRegistry.writeStringValue(
				    WinRegistry.HKEY_CURRENT_USER,                             //HKEY
				   "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run",           //Key
				   "GifAnything",
				   "javaw -jar " + Main.class.getProtectionDomain().getCodeSource().getLocation().getFile().substring(1),
				   WinRegistry.KEY_WOW64_64KEY);						  
			} else if(os.contains("linux")){
				File homeDir = new File(System.getProperty("user.home"));
				File startupDir = new File(homeDir, ".config/autostart/");
				File desktopFile = new File(startupDir,"GifAnything.desktop");
				
				if(desktopFile.exists()){
					desktopFile.delete();
				}
				
				PrintWriter desktopFileOutput = new PrintWriter(desktopFile);
				desktopFileOutput.println("[Desktop Entry]");
				desktopFileOutput.println("Type=Application");
				desktopFileOutput.println("Name=GifAnything");
				desktopFileOutput.println("Exec=java -jar " + Main.class.getProtectionDomain().getCodeSource().getLocation().getFile());
				desktopFileOutput.close();	
				desktopFile.setExecutable(true);
			
			} else{
				throw new IllegalStateException("This feature is not supported on your operating system...yet");
			}

		}
		else
		{
			if(os.contains("windows")){
				WinRegistry.deleteValue(
				    WinRegistry.HKEY_CURRENT_USER,                             //HKEY
				   "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run",           //Key
				   "GifAnything",
				   WinRegistry.KEY_WOW64_64KEY);
			} else if(os.contains("linux")){
				File homeDir = new File(System.getProperty("user.home"));
				File startupDir = new File(homeDir, ".config/autostart/");
				
				File desktopFile = new File(startupDir,"GifAnythig.desktop");
				
				if(desktopFile.exists()){
					desktopFile.delete();
				}
			} else{
				throw new IllegalStateException("This feature is not supported on your operating system...yet");
			}
		} 
	}

	public static boolean isStartupItemSupported(){
		String os = System.getProperty("os.name").toLowerCase();
		boolean returnVal = false;
		if(os.contains("windows")){
			returnVal = true;
		} else if(os.contains("linux")){
			returnVal = true;
		}
		
		return returnVal;
	}

	public static boolean isStartupItem(){
		String os = System.getProperty("os.name").toLowerCase();
		boolean returnVal = false;
		if(os.contains("windows")){
			try {
				String s = WinRegistry.readString(
				    WinRegistry.HKEY_CURRENT_USER,                             //HKEY
				   "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run",           //Key
				   "GifAnything",
				   WinRegistry.KEY_WOW64_64KEY);
				
				returnVal = s != null;
			} catch (Exception e) {
				returnVal = false;
			}
		} else if(os.contains("linux")){
			File homeDir = new File(System.getProperty("user.home"));
			File startupDir = new File(homeDir, ".config/autostart/");
			File desktopFile = new File(startupDir,"GifAnything.desktop");
			
			returnVal = (desktopFile.exists());
		} else {
			returnVal = false;
		}
		
		return returnVal;
	}

}
