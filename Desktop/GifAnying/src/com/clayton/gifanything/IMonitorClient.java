package com.clayton.gifanything;

public interface IMonitorClient{
	public void startTrigger();
	public void stopTrigger();
}